<?php

    require("classes/Film.php");
    require("models/dbConnection.php");
    require("libs/getFilmList.php");
    require("libs/showFilmList.php");
    require("views/filmInfoInFilmList.php");

    $page = $_POST["page"];
    $department = $_POST["department"];

    $posterButtonTitle = "АФИША И РАСПИСАНИЕ";
    $soonButtonTitle = "БЛИЖАЙШИЕ ПРЕМЬЕРЫ";
    $contactsButtonTitle = "КОНТАКТЫ";

    if(($department == "ela") || ($department == "")){
        $department = "ela";
        $departmentID = 1;
        $title = "Кинотеатр BROOKLYN | Елабуга";
        $changeDepartment = "zel";
        $changeDepartmentButtonTitle = "Я из Зеленодольска";
    }elseif($department == "zel"){
        $departmentID = 2;
        $title = "Кинотеатр BROOKLYN | Зеленодольск";
        $changeDepartment = "ela";
        $changeDepartmentButtonTitle = "Я из Елабуги";
    }

    require("views/page-top.php");
    require("views/head.php");
    require("views/body-begin.php");
    require("views/header.php");

    switch($page){
        case "poster":
            echo("<h1>Poster</h1>\n");
            require("models/poster.php");
            break;
        case "soon":
            echo("<h1>Soon</h1>\n");
            break;
        case "contacts":
            echo("<h1>Contacts</h1>\n");
            break;
        case "index":
        default:
            echo("<h1>Index</h1>\n");
            break;
    }

    require("views/footer.php");
    require("views/body-end.php");
    require("views/page-bottom.php");




    // require("views/main.php");

?>