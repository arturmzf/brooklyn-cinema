<?php

class Film {

    private $id;
    private $number;
    private $title;
    private $rating;
    private $genre;
    private $sessionDuration;
    private $trailersDuration;
    private $plot;
    private $cast;
    private $slogan;
    private $releaseDate;
    private $isPremier;
    private $kinoPoiskLink;
    private $kinoPlanLink;
    private $distributor;
    private $has3D;

    function __construct(
                    $id = 0, $number=0, $title = "Title", $rating = 0,
                    $genre = "Genre", $sessionDuration = 0, $trailersDuration = 0,
                    $plot = "Plot", $cast = "Cast", $slogan = "Slogan",
                    $releaseDate = "2022-01-01", $isPremier = 0, $kinoPoiskLink = "KinoPoisk Link",
                    $kinoPlanLink = "KinoPlan Link", $distributor = "Distributor", $has3D = 0){

        $this->id = $id;
        $this->number = $number;
        $this->title = $title;
        $this->rating = $rating;
        $this->genre = $genre;
        $this->sessionDuration = $sessionDuration;
        $this->trailersDuration = $trailersDuration;
        $this->plot = $plot;
        $this->cast = $cast;
        $this->slogan = $slogan;
        $this->releaseDate = $releaseDate;
        $this->isPremier = $isPremier;
        $this->kinoPoiskLink = $kinoPoiskLink;
        $this->kinoPlanLink = $kinoPlanLink;
        $this->distributor = $distributor;
        $this->has3D = $has3D;

    }

    public function setId($id){
        $this->id = $id;
    }

    public function getId(){
        return $this->id;
    }

    public function setNumber($number) {
        $this->number = $number;
    }

    public function getNumber() {
        return $this->number;
    }

    public function setTitle($title){
        $this->title = $title;
    }

    public function getTitle(){
        return $this->title;
    }

    public function setRating($rating){
        $this->rating = $rating;
    }

    public function getRating(){
        return $this->rating;
    }

    public function setGenre($genre){
        $this->genre = $genre;
    }

    public function getGenre(){
        return $this->genre;
    }

    public function setSessionDuration($sessionDuration){
        $this->sessionDuration = $sessionDuration;
    }

    public function getSessionDuration(){
        return $this->sessionDuration;
    }

    public function setTrailersDuration($trailersDuration){
        $this->trailersDuration = $trailersDuration;
    }

    public function getTrailersDuration(){
        return $this->trailersDuration;
    }
}

?>
