<?php

    function getFilmList($dbConnection, $departmentID){

        $sql = "SELECT FILMS_ID FROM timetables WHERE DEPARTMENTS_ID = '".$departmentID."' 
                    GROUP BY FILMS_ID ORDER BY FILMS_ID DESC;";
        $filmIDResource = mysqli_query($dbConnection, $sql);
        $filmsAmount = mysqli_num_rows($filmIDResource);
        $filmList = Array($filmsAmount);

        for($i = 0; $i < $filmsAmount; $i++){

            $filmID = mysqli_fetch_assoc($filmIDResource);

            $sql = "SELECT TITLE, RATING FROM FILMS WHERE ID = '".$filmID["FILMS_ID"]."';";
            $filmInfoAssoc = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql));

            $film = new Film;
            $film->setId($filmID["FILMS_ID"]);
            $film->setTitle($filmInfoAssoc["TITLE"]);
            $film->setRating($filmInfoAssoc["RATING"]);

            $filmList[$i] = $film;

        }

        return $filmList;

    }





?>
