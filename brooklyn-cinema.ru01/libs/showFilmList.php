<?php

    function showFilmList($filmList, $department){

        $filmsAmount = count($filmList);

        for($i = 0; $i < $filmsAmount; $i++){

            $film = $filmList[$i];

            $id = $film->getId();
            $title = $film->getTitle();
            $rating = $film->getRating();

            if(empty($title)){
                continue;
            }

            showFilm($id, $title, $rating, $department);

        }

    }

?>
