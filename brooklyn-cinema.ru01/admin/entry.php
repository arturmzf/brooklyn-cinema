<?php
    echo <<< BEGIN
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>$title</title>
        <link rel="stylesheet" href="../resources/brooklyn-main.css" />
		<link rel="shortcut icon" type="image/png" href="../resources/logo-bw.png"/>
    </head>
    <body>
        <header class="admin">
            <form action="rqleve4.php" method="post">
                <button name="main" value="index">
                    <img class="logoimg" src="../resources/logo.png" />
                </button>
            </form>
            <span class="admin-panel"> | Панель администратора</span>
        </header>
        <div class="main-container admin">
            <div class="container-0">
                <br />
                <h1>$header</h1>
                <br />
BEGIN;
    switch($mainField) {
        case "main":
            require "main.php";
            break;
        // Список текущих фильмов
        case "filmList":
            $dbConnection = mysqli_connect(DB_HOST, DB_LOGIN, DB_PASS, DB_TITLE);

            if($toDo == "removefilm"){

                $sql11 = "DELETE FROM films WHERE ID = '".$filmID."';";
                $sql12 = "DELETE FROM timetables WHERE FILMS_ID = '".$filmID."';";
                mysqli_query($dbConnection, $sql11);
                mysqli_query($dbConnection, $sql12);

            }elseif($toDo == "changefilmsorder"){

                $sql14 = "UPDATE films SET films_order = '".$filmsOrder."' WHERE ID = '".$filmID."';";
                mysqli_query($dbConnection, $sql14);

            }elseif($toDo == "setpremier"){

                $sql15 = "UPDATE films SET is_premier = '1' WHERE ID = '".$filmID."';";
                mysqli_query($dbConnection, $sql15);

            }elseif($toDo == "setnotpremier"){

                $sql16 = "UPDATE films SET is_premier = '0' WHERE ID = '".$filmID."';";
                mysqli_query($dbConnection, $sql16);

            }elseif($toDo == "changeSessionDuration"){

                $sessionDurationAbsolute = ($sessionDurationHours * 60 * 60) + ($sessionDurationMinutes * 60);

                $sql17 = "UPDATE films SET session_duration = '".$sessionDurationAbsolute."' WHERE ID = '".$filmID."';";
                mysqli_query($dbConnection, $sql17);

            }
            elseif($toDo == "changeTGLink"){

                $sql18 = "UPDATE films SET tg_link = '".$tgLink."' WHERE ID = '".$filmID."';";
                mysqli_query($dbConnection, $sql18);

            }elseif($toDo == "createShortCaption"){

                require("shortFilmCaptionsCreating.php");

            }elseif($toDo == "createMediumCaption"){

                require("mediumFilmCaptionsCreating.php");

            }

            require "filmList.php";
            break;
        // Список текущих расписаний
        case "timetableList":
            $dbConnection = mysqli_connect(DB_HOST, DB_LOGIN, DB_PASS, DB_TITLE);

            if(($toDo == "createwiki") || ($toDo == "createtextview")
                    || ($toDo == "createTGTextView") || ($toDo == "createfilmcards")){
                $resultWiki = "";
                $resultTextView = "";

                $sql03 = "SELECT DATE_START, DATE_END FROM dates WHERE ID = '".$datesID."';";
                $datesAssoc = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql03));
                $dateStart = $datesAssoc["DATE_START"];
                $dateEnd = $datesAssoc["DATE_END"];

                $dayStart = mb_substr($dateStart, 8, 2);
                $mounthStart = mb_substr($dateStart, 5, 2);
                $yearStart = mb_substr($dateStart, 0, 4);

                $dayEnd = mb_substr($dateEnd, 8, 2);
                $mounthEnd = mb_substr($dateEnd, 5, 2);
                $yearEnd = mb_substr($dateEnd, 0, 4);

                $mounthStartString = "";
                $mounthEndString = "";

                switch($mounthStart){
                    case "01":
                        $mounthStartString = "января";
                        break;
                    case "02":
                        $mounthStartString = "февраля";
                        break;
                    case "03":
                        $mounthStartString = "марта";
                        break;
                    case "04":
                        $mounthStartString = "апреля";
                        break;
                    case "05":
                        $mounthStartString = "мая";
                        break;
                    case "06":
                        $mounthStartString = "июня";
                        break;
                    case "07":
                        $mounthStartString = "июля";
                        break;
                    case "08":
                        $mounthStartString = "августа";
                        break;
                    case "09":
                        $mounthStartString = "сентября";
                        break;
                    case "10":
                        $mounthStartString = "октября";
                        break;
                    case "11":
                        $mounthStartString = "ноября";
                        break;
                    case "12":
                        $mounthStartString = "декабря";
                        break;
                    default:
                        $mounthStartString = "НЕОПРЕДЕЛЁННОГО_МЕСЯЦА";
                        break;
                }

                switch($mounthEnd){
                    case "01":
                        $mounthEndString = "января";
                        break;
                    case "02":
                        $mounthEndString = "февраля";
                        break;
                    case "03":
                        $mounthEndString = "марта";
                        break;
                    case "04":
                        $mounthEndString = "апреля";
                        break;
                    case "05":
                        $mounthEndString = "мая";
                        break;
                    case "06":
                        $mounthEndString = "июня";
                        break;
                    case "07":
                        $mounthEndString = "июля";
                        break;
                    case "08":
                        $mounthEndString = "августа";
                        break;
                    case "09":
                        $mounthEndString = "сентября";
                        break;
                    case "10":
                        $mounthEndString = "октября";
                        break;
                    case "11":
                        $mounthEndString = "ноября";
                        break;
                    case "12":
                        $mounthEndString = "декабря";
                        break;
                    default:
                        $mounthEndString = "НЕОПРЕДЕЛЁННОГО_МЕСЯЦА";
                        break;
                }

                if($yearStart != $yearEnd){
                    $period = "с ".$dayStart." ".$mounthStartString." ".$yearStart." года по ".$dayEnd." ".$mounthEndString." ".$yearEnd." года";
                } else{
                    if($mounthStart != $mounthEnd){
                        $period = "с ".$dayStart." ".$mounthStartString." по ".$dayEnd." ".$mounthEndString." ".$yearEnd." года";
                    }else{
                        if($dayStart != $dayEnd){
                            $period = "с ".$dayStart." по ".$dayEnd." ".$mounthStartString." ".$yearStart." года";
                        }else{
                            $period = $dayStart." ".$mounthStartString." ".$yearStart." года";
                        }
                    }
                }

                $sql04 = "SELECT TITLE_RU, ABBREVIATION, ADS_DURATION, COME_BACK_BUTTON_TOP,
                                                COME_BACK_BUTTON_BOTTOM, MAIN_MENU_PAGE_LINK, DELIMITER
                                                FROM departments WHERE ID = '".$departmentsID."';";
                $departmentInfo = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql04));
                $departmentTitleRu = $departmentInfo["TITLE_RU"];
                $abbreviation = $departmentInfo["ABBREVIATION"];
                $adsDuration = $departmentInfo["ADS_DURATION"];
                $comeBackButtonTop = $departmentInfo["COME_BACK_BUTTON_TOP"];
                $comeBackButtonBottom = $departmentInfo["COME_BACK_BUTTON_BOTTOM"];
                $MainMenuPageLink = $departmentInfo["MAIN_MENU_PAGE_LINK"];
                $delimiter = $departmentInfo["DELIMITER"];

                switch($toDo){
                    case "createwiki":
                        require("timetableWiki.php");
                        break;

                    case "createtextview":
                        require("timetableTextView.php");
                        break;

                    case "createTGTextView":
                        require("timetableTGTextView.php");
                        break;

                    case "createfilmcards":
                        require("timetableFilmCardsCreating.php");
                        break;

                    default:
                        // NOTHING
                        break;
                }
            }elseif($toDo == "createimgstrings"){

                $sql05 = "SELECT FILMS_ID FROM timetables WHERE DEPARTMENTS_ID = '".$departmentsID."' AND
                                    DATES_ID = '".$datesID."' GROUP BY FILMS_ID;";
                $filmsListObject = mysqli_query($dbConnection, $sql05);
                $filmsAmount = mysqli_num_rows($filmsListObject);

                $sql09 = "SELECT DATE_START, DATE_END FROM dates WHERE ID = '".$datesID."';";
                $datesAssoc = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql09));
                $dateStart = $datesAssoc["DATE_START"];
                $dateEnd = $datesAssoc["DATE_END"];

                $dayStart = mb_substr($dateStart, 8, 2);
                $mounthStart = mb_substr($dateStart, 5, 2);
                $yearStart = mb_substr($dateStart, 0, 4);

                for($c = 0; $c < $filmsAmount; $c++){

                    $filmID = mysqli_fetch_assoc($filmsListObject)["FILMS_ID"];

                    $sql06 = "SELECT title, rating, genre, has_3d, session_duration
                                        FROM films WHERE ID = '".$filmID."';";
                    $filmInfoAssoc = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql06));

                    $filmTitle = $filmInfoAssoc["title"];

                    $filmRating = $filmInfoAssoc["rating"];
                    $filmRatingString = $filmInfoAssoc["rating"]."+";
                    $ratingSpecCol = 0;
                    if($filmRating == 18){
                        $ratingSpecCol = 1;
                    }

                    $filmGenre = $filmInfoAssoc["genre"];
                    $has3D = $filmInfoAssoc["has_3d"];

                    $sessionDurationAbsolute = $filmInfoAssoc["session_duration"];
                    $sessionDurationHours = (int)($sessionDurationAbsolute / 60 / 60);
                    $sessionDurationMinutes = (int)($sessionDurationAbsolute / 60 % 60);

                    if($sessionDurationMinutes == 0){
                        if($sessionDurationHours == 0){
                            $sessionDurationString = "нет данных";
                        }else{
                            $sessionDurationString = $sessionDurationHours." ч. ровно";
                        }
                    }else{
                        if($sessionDurationHours == 0){
                            $sessionDurationString = $sessionDurationMinutes." мин.";
                        }else{
                            $sessionDurationString = $sessionDurationHours." ч. ".$sessionDurationMinutes." мин.";
                        }
                    }

                    $sessionDurationString = mb_strtoupper($sessionDurationString);

                    if($filmTitle == null){
                        continue;
                    }

                    $sql07 = "SELECT ABBREVIATION, ABBREVIATION_RU, CARD_FOR_COMMON_TT FROM departments WHERE ID = '".$departmentsID."';";
                    $departmentInfoAssoc = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql07));
                    $abbreviation = $departmentInfoAssoc["ABBREVIATION"];
                    $abbreviationRu = $departmentInfoAssoc["ABBREVIATION_RU"];
                    $imagePath = $departmentInfoAssoc["CARD_FOR_COMMON_TT"];

                    $sql08 = "SELECT TIME_BEGIN, HALLS_ID, IS_3D FROM timetables
                                        WHERE FILMS_ID = '".$filmID."' AND
                                                DEPARTMENTS_ID = '".$departmentsID."' AND
                                                DATES_ID='".$datesID."' ORDER BY TIME_BEGIN;";
                    $sessionInfoObject1 = mysqli_query($dbConnection, $sql08);
                    $sessionInfoObject2 = mysqli_query($dbConnection, $sql08);
                    $sessionInfoObject3 = mysqli_query($dbConnection, $sql08);
                    $sessionsAmount = mysqli_num_rows($sessionInfoObject1);

                    $existence3D = 0;
                    for($i = 0; $i < $sessionsAmount; $i++){

                        $sessionInfoAssoc = mysqli_fetch_assoc($sessionInfoObject1);

                        $timeBegin = $sessionInfoAssoc["TIME_BEGIN"];
                        $hallsID = $sessionInfoAssoc["HALLS_ID"];
                        $is3D = $sessionInfoAssoc["IS_3D"];

                        if($timeBegin == null){
                            continue;
                        }

                        $existence3D += $is3D;

                    }

                    $format2D = "2D | ";
                    $format3D = "";
                    $pipeSeparator = "";
                    if(($has3D == 1) && ($existence3D > 0)){
                        $format2D = "2D | ";
                        $format3D = "3D";
                        $pipeSeparator = " | ";
                    }

                    require("timetableImgStrings.php");

                }

            }elseif($toDo == "removetimetable"){

                $sql10 = "DELETE FROM timetables WHERE DEPARTMENTS_ID = '".$departmentsID."' AND
                                            DATES_ID = '".$datesID."';";
                mysqli_query($dbConnection, $sql10);

            }

            require "timetableList.php";
            break;
        // Редактирование конкретного расписания
        case "timetableEdit":
            $dbConnection = mysqli_connect(DB_HOST, DB_LOGIN, DB_PASS, DB_TITLE);

            if(($toDo == "addsession") && (!empty($filmID))) {

                $timeBegin = $sessionStartHour.":".$sessionStartMinute.":00";
                $timeBeginAbsolute = $sessionStartHour*60 + $sessionStartMinute;

                $timeEndAbsolute = $timeBeginAbsolute + ($sessionDuration / 60);
                $sessionEndHour = (int)(($timeEndAbsolute / 60) % 24);
                $sessionEndMinute = $timeEndAbsolute % 60;

                $timeEnd = $sessionEndHour.":".$sessionEndMinute.":00";

                $sql01 = "INSERT INTO timetables (TIME_BEGIN, TIME_END, FILMS_ID, IS_3D, DEPARTMENTS_ID, HALLS_ID, DATES_ID)
                                VALUES ('".$timeBegin."', '".$timeEnd."', '".$filmID."', '".$is3D."', '".$departmentsID."', '".$sessionHall."', '".$datesID."');";
                mysqli_query($dbConnection, $sql01);

                //echo($sql01);
            }elseif($toDo == "rmvFilmFromTTB"){

                $sql13 = "DELETE FROM timetables WHERE DATES_ID = '".$datesID."' AND FILMS_ID = '".$filmID."'
                                AND DEPARTMENTS_ID = '".$departmentsID."' AND IS_3D = '".$is3D."';";
                mysqli_query($dbConnection, $sql13);

            }elseif($toDo == "rmvsession"){

                $sql13 = "DELETE FROM timetables WHERE ID = '".$sessionID."';";
                mysqli_query($dbConnection, $sql13);

            }elseif(($toDo == "addfilmtotimetable") && (!empty($filmID))){
                $sql02 = "INSERT INTO timetables (FILMS_ID, IS_3D, DEPARTMENTS_ID, DATES_ID)
                                VALUES ('".$filmID."', '".$is3D."', '".$departmentsID."', '".$datesID."');";
                mysqli_query($dbConnection, $sql02);
            }

            require "timetableEdit.php";
            break;
        // Нет полей... Ошибка!
        default:
            echo("ERROR!");
            break;
        }
        echo <<< END
            </div>
        </div>
    </body>
</html>
END;
?>
