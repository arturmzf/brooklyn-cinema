<?php
    if(!empty($_POST["startday"]) && !empty($_POST["startmounth"]) && !empty($_POST["startyear"]) &&
            !empty($_POST["endday"]) && !empty($_POST["endmounth"]) && !empty($_POST["endyear"])){

        $dateStart = $_POST["startyear"]."-".$_POST["startmounth"]."-".$_POST["startday"];
        $dateEnd = $_POST["endyear"]."-".$_POST["endmounth"]."-".$_POST["endday"];

        $departmentsID = $_POST["department"];

        /*
        // Testing
        echo($dateStart);
        echo($dateEnd);
        */

        $sql01 = "INSERT INTO dates (DATE_START, DATE_END) VALUES ('".$dateStart."', '".$dateEnd."');";
        $sql03 = "SELECT ID FROM dates WHERE DATE_START = '".$dateStart."' AND DATE_END = '".$dateEnd."';";
        mysqli_query($dbConnection, $sql01);
        $datesID = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql03));

        $sql04 = "INSERT INTO timetables (DEPARTMENTS_ID, DATES_ID) VALUES ('".$departmentsID."', '".$datesID["ID"]."');";
        mysqli_query($dbConnection, $sql04);
    }

    echo("<b>Добавление нового расписания</b>");
    echo("<br />");
    echo("<br />");

    echo <<< PART01
        <form action="rqleve4.php" method="POST">
            Введите промежуток дат и город:<br />
            Начало - 
            <input type="text" name="startday" size="2" maxlength="2" value="01" />
            <select name="startmounth" size="1" >
                <option value="01">Января</option>
                <option value="02">Февраля</option>
                <option value="03">Марта</option>
                <option value="04">Апреля</option>
                <option value="05">Мая</option>
                <option value="06">Июня</option>
                <option value="07">Июля</option>
                <option value="08">Августа</option>
                <option value="09">Сентября</option>
                <option value="10">Октября</option>
                <option value="11">Ноября</option>
                <option value="12">Декабря</option>
            </select>
            <input type="text" name="startyear" size="4" maxlength="4" value="2023" /><br />
            Окончание - 
            <input type="text" name="endday" size="2" maxlength="2" value="01" />
            <select name="endmounth" size="1" >
                <option value="01">Января</option>
                <option value="02">Февраля</option>
                <option value="03">Марта</option>
                <option value="04">Апреля</option>
                <option value="05">Мая</option>
                <option value="06">Июня</option>
                <option value="07">Июля</option>
                <option value="08">Августа</option>
                <option value="09">Сентября</option>
                <option value="10">Октября</option>
                <option value="11">Ноября</option>
                <option value="12">Декабря</option>
            </select>
            <input type="text" name="endyear" size="4" maxlength="4" value="2023" /><br />
            <br />
            Город -
            <select name="department" size="1" >
                <option value="1">Елабуга</option>
                <option value="2">Зеленодольск</option>
            </select>
            <br />
            <br />
            <button name="main" value="timetableList">Добавить расписание</button>
        </form>
        <br />
        <br />
PART01;

    $sql02 = "SELECT DATES_ID, DEPARTMENTS_ID FROM timetables GROUP BY DATES_ID, DEPARTMENTS_ID;";
    $datesListObject = mysqli_query($dbConnection, $sql02);
    $datesListRows = mysqli_num_rows($datesListObject);
    // echo(mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT));
    // mysqli_close($dbConnection);

    for($i = 0; $i < $datesListRows; $i++) {

        $datesListResult = mysqli_fetch_assoc($datesListObject);

        $dateStart = "";
        $dateEnd = "";
        $department = "";

        if(!empty($datesListResult["DATES_ID"]) && !empty($datesListResult["DEPARTMENTS_ID"])){

            $sql05 = "SELECT DATE_START, DATE_END FROM dates WHERE ID = '".$datesListResult["DATES_ID"]."';";
            $sql06 = "SELECT TITLE_RU FROM departments WHERE ID = '".$datesListResult["DEPARTMENTS_ID"]."';";

            if($datesListResult["DEPARTMENTS_ID"] == 1){
                $vkPublikLink = "photo-3324177";
                //$vkPublikLink = "photo-3324177_xxxxxxxxx";
            }else{
                $vkPublikLink = "photo-44334955";
                //$vkPublikLink = "photo-44334955_xxxxxxxxx";
            }

            $datesListResult1 = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql05));
            $datesListResult2 = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql06));
            $whatTimetable = $datesListResult1["DATE_START"]." - ".$datesListResult1["DATE_END"]." - ".$datesListResult2["TITLE_RU"];

            echo("<div class=\"cards\">\n");
            echo("<div class=\"pre-wrap\"><h3>".$datesListResult1["DATE_START"]." | ".$datesListResult1["DATE_END"]." | ".$datesListResult2["TITLE_RU"]."</h3></div>");
            // echo("<br />\n");
            echo("<form action=\"rqleve4.php\" method=\"post\">");
            echo("<button name=\"main\" value=\"timetableEdit\">Посмотреть/Отредактировать это расписание</button><br />\n");
            echo("<input type=\"hidden\" name=\"whatTimetable\" value=\"".$whatTimetable."\" />");
            echo("<input type=\"hidden\" name=\"datesID\" value=\"".$datesListResult["DATES_ID"]."\" />");
            echo("<input type=\"hidden\" name=\"departmentsID\" value=\"".$datesListResult["DEPARTMENTS_ID"]."\" />");
            echo("</form>");
            echo("<form class=\"rmv\" action=\"rqleve4.php\" method=\"post\">");
            echo("<button name=\"main\" value=\"timetableList\">Удалить это расписание</button><br />\n");
            echo("<input type=\"hidden\" name=\"whatTimetable\" value=\"".$whatTimetable."\" />");
            echo("<input type=\"hidden\" name=\"datesID\" value=\"".$datesListResult["DATES_ID"]."\" />");
            echo("<input type=\"hidden\" name=\"departmentsID\" value=\"".$datesListResult["DEPARTMENTS_ID"]."\" />");
            echo("<input type=\"hidden\" name=\"todo\" value=\"removetimetable\" />");
            echo("</form>");
            echo("<br />\n");
            echo("<form action=\"rqleve4.php\" method=\"post\">\n");
            echo("<h4>Создание Wiki-разметки расписания для VK</h4>\n");
            echo("Адрес фотографии для кнопки: \n");
            echo("<input type=\"text\" name=\"vkenterbutton\" size=\"30\" maxlength=\"50\" placeholder=\"".$vkPublikLink."_xxxxxxxxx\" \n/><br />\n");
            echo("Адрес фотографии для заголовка: \n");
            echo("<input type=\"text\" name=\"vktimetableheader\" size=\"30\" maxlenght=\"50\" placeholder=\"".$vkPublikLink."_xxxxxxxxx\" />\n<br />\n");
            echo("<button name=\"main\" value=\"timetableList\">Создать Wiki-разметку для VK</button><br />\n");
            echo("<input type=\"hidden\" name=\"datesID\" value=\"".$datesListResult["DATES_ID"]."\" />");
            echo("<input type=\"hidden\" name=\"departmentsID\" value=\"".$datesListResult["DEPARTMENTS_ID"]."\" />");
            echo("<input type=\"hidden\" name=\"todo\" value=\"createwiki\" />");
            echo("</form>\n");
            echo("<br />\n");
            echo("<form action=\"rqleve4.php\" method=\"post\">\n");
            echo("<h4>Создание общего текстового варианта расписания</h4>\n");
            echo("<button name=\"main\" value=\"timetableList\">Создать общий текстовый вариант расписания</button><br />\n");
            echo("<input type=\"hidden\" name=\"datesID\" value=\"".$datesListResult["DATES_ID"]."\" />");
            echo("<input type=\"hidden\" name=\"departmentsID\" value=\"".$datesListResult["DEPARTMENTS_ID"]."\" />");
            echo("<input type=\"hidden\" name=\"todo\" value=\"createtextview\" />");
            echo("</form>\n");
            echo("<br />\n");
            echo("<form action=\"rqleve4.php\" method=\"post\">\n");
            echo("<h4>Создание текстового варианта расписания для Телеграм</h4>\n");
            echo("<button name=\"main\" value=\"timetableList\">Создать текстовый вариант расписания для Телеграм</button><br />\n");
            echo("<input type=\"hidden\" name=\"datesID\" value=\"".$datesListResult["DATES_ID"]."\" />");
            echo("<input type=\"hidden\" name=\"departmentsID\" value=\"".$datesListResult["DEPARTMENTS_ID"]."\" />");
            echo("<input type=\"hidden\" name=\"todo\" value=\"createTGTextView\" />");
            echo("</form>\n");
            echo("<br />\n");
            echo("<form action=\"rqleve4.php\" method=\"post\">\n");
            echo("<h4>Создание информационных карточек для фильмов с данным расписанием</h4>\n");
            echo("<button name=\"main\" value=\"timetableList\">Создать информационные карточки</button><br />\n");
            echo("<input type=\"hidden\" name=\"datesID\" value=\"".$datesListResult["DATES_ID"]."\" />");
            echo("<input type=\"hidden\" name=\"departmentsID\" value=\"".$datesListResult["DEPARTMENTS_ID"]."\" />");
            echo("<input type=\"hidden\" name=\"todo\" value=\"createfilmcards\" />");
            echo("</form>\n");
            echo("<br />\n");
            echo("<form action=\"rqleve4.php\" method=\"post\">\n");
            echo("<h4>Создание графических полосок для каждого фильма</h4>\n");
            echo("<button name=\"main\" value=\"timetableList\">Создать графические полоски</button><br />\n");
            echo("<input type=\"hidden\" name=\"datesID\" value=\"".$datesListResult["DATES_ID"]."\" />");
            echo("<input type=\"hidden\" name=\"departmentsID\" value=\"".$datesListResult["DEPARTMENTS_ID"]."\" />");
            echo("<input type=\"hidden\" name=\"todo\" value=\"createimgstrings\" />");
            echo("</form>\n");
            echo("</div>\n");
            echo("<br />\n");


        } else{
            echo("<p>Расписаний пока нет...</p><br />");
        }


    }

    echo("<br />");
    echo("<form action=\"rqleve4.php\" method=\"post\">");
    echo("  <button name=\"main\" value=\"\">Обратно на главную</button>");
    echo("</form>");
    echo("<br />");

?>