<?php
    /*
    define("DB_HOST", "localhost");
    define("DB_LOGIN", "root");
    define("DB_PASS", "rQLevE4");
    define("DB_TITLE", "brooklyncinema");

    $dbConnection = mysqli_connect(DB_HOST, DB_LOGIN, DB_PASS, DB_TITLE);
    */

    if(!empty($_POST["title"])){

        $title = $_POST["title"];
        $filmsOrder = $_POST["filmsorder"];
        $rating = 0;
        $format = 0;
        $isPremier = 0;

        switch($_POST["rating"]){
            case 6:
                $rating = 6;
                break;
            case 12:
                $rating = 12;
                break;
            case 16:
                $rating = 16;
                break;
            case 18:
                $rating = 18;
                break;
            default:
                $rating = 0;
                break;
        }

        $kinoPoiskLink = $_POST["kinopoisklink"];
        $kinoPlanLink = $_POST["kinoplanlink"];
        $articlesLinkEnd = $_POST["articleslinkend"];
        $genre = $_POST["genre"];
        $tgLink = "https://t.me/brooklynposter/".$_POST["tgnumber"];

        if($_POST["format"] == 3){
            $format = 1;
        }

        if($_POST["premier"] == 1){
            $isPremier = 1;
        }

        $releaseDate = $_POST["releasedate"];

        $sessionDuration = ((int)$_POST["sessiondurationhours"] * 60 * 60) + ((int)$_POST["sessiondurationminutes"] * 60);
        $trailersDuration = ((int)$_POST["trailersdurationminutes"] * 60) + ((int)$_POST["trailersdurationseconds"]);

        //$plot = $_POST["plot"];
        $plot = htmlentities($_POST["plot"]);
        $cast = $_POST["cast"]." и др.";;
        $slogan = $_POST["slogan"];
        $distributor = $_POST["distributor"];

        $sql01 = "INSERT INTO films (films_order, title, rating, kinopoisk_link, kinoplan_link, articles_link_end, tg_link, is_premier, release_date,
                                            genre, has_3d, session_duration, trailers_duration,
                                            plot, cast, slogan, distributor)
                                VALUES ('".$filmsOrder."', '".$title."', '".$rating."', '".$kinoPoiskLink."', '".$kinoPlanLink."', '".$articlesLinkEnd."', '".$tgLink."', '".$isPremier."', '".$releaseDate."' ,'".
                                        $genre."', '".$format."', '".$sessionDuration."', '".$trailersDuration."', '".
                                        $plot."', '".$cast."', '".$slogan."', '".$distributor."');";

        mysqli_query($dbConnection, $sql01);

        // header("Location: rqleve4.php");
        // exit;
    }


    echo <<< PART01
        <b>Добавление нового продукта</b>
        <br />
        <br />
        <form action="rqleve4.php" method="POST">
            Введите наименование фильма без кавычек:<br />
            <input type="text" name="title" size="50" maxlength="100" placeholder="Наименование"  /><br /><br />
            Укажите желаемый номер фильма в очереди:<br />
            <input type="text" name="filmsorder" size="3" maxlength="3" placeholder="0"  /><br /><br />
            Введите ссылку на страницу фильма на КиноПоиске:<br />
            <input type="text" name="kinopoisklink" size="50" maxlength="100" placeholder="https://www.kinopoisk.ru/" /><br /><br />
            Введите ссылку на страницу фильма на КиноПлане:<br />
            <input type="text" name="kinoplanlink" size="50" maxlength="100" placeholder="https://www.kinoplan.ru/" /><br /><br />
            Введите окончание ссылки на ВК-статью с информацией о фильме:<br />
            <input type="text" name="articleslinkend" size="50" maxlength="100" placeholder="0000" /><br /><br />
            Введите предполагаемый номер поста описания в Телеграм:<br />
            <input type="text" name="tgnumber" size="50" maxlength="100" placeholder="tg" /><br /><br />
            Укажите возрастную категорию:<br />
            <select name="rating" size="1">
                <option value="0">0+</option>
                <option value="6">6+</option>
                <option value="12">12+</option>
                <option value="16">16+</option>
                <option value="18">18+</option>
            </select><br /><br />
            Укажите форматы показа:<br />
            <select name="format" size="1">
                <option value="2">только 2D</option>
                <option value="3">2D и 3D</option>
            </select><br /><br />
            Является ли показ премьерным?<br />
            <input type="checkbox" name="premier" value="1" /> Да, это премьера<br /><br />
            Укажите дату релиза в формате YYYY-MM-DD:<br />
            <input type="text" name="releasedate" size="50" maxlength="100" placeholder="YYYY-MM-DD" /><br /><br />
            Введите жанры фильма:<br />
            <input type="text" name="genre" size="50" maxlength="100" placeholder="Жанр" /><br /><br />
            Введите продолжительность фильма в часах и минутах:<br />
            <input type="text" name="sessiondurationhours" size="2" maxlength="2" placeholder="00" /> ч.
            <input type="text" name="sessiondurationminutes" size="2" maxlength="2" placeholder="00" /> мин.<br /><br />
            Введите продолжительность блока с трейлерами в минутах и секундах:<br />
            <input type="text" name="trailersdurationminutes" size="2" maxlength="2" placeholder="00" /> мин.
            <input type="text" name="trailersdurationseconds" size="2" maxlength="2" placeholder="00" /> сек.<br /><br />
            Cюжет/описание фильма:<br />
            <textarea rows="10" cols="45" name="plot" placeholder="Сюжет"></textarea><br />
            <br />
            В ролях:<br />
            <textarea rows="10" cols="45" name="cast" placeholder="Актёры"></textarea><br />
            <br />
            Слоган фильма:<br />
            <textarea rows="1" cols="45" name="slogan" placeholder="Слоган фильма"></textarea><br />
            <br />
            Дистрибьютор:<br />
            <textarea rows="1" cols="45" name="distributor" placeholder="Дистрибьютор"></textarea>
            <br />
            <br />
            <!-- <input type="submit" value="Добавить фильм" /> -->
            <!-- <input type="hidden" name="main" value="filmList" /> -->
            <button name="main" value="filmList">Добавить фильм</button>
        </form>
        <br />
        <form action="rqleve4.php" method="post">
            <button name="main" value="">Обратно на главную</button>
        </form>
        <br />
        <br />

PART01;

    $sql02 = "SELECT * FROM films ORDER BY films_order";
    $filmListObject = mysqli_query($dbConnection, $sql02);
    $filmsAmount = mysqli_num_rows($filmListObject);
    mysqli_close($dbConnection);

    echo("<b>Количество фильмов в базе: ".$filmsAmount."</b><br />");
    echo("<br />");
    for($i = 0; $i < $filmsAmount; $i++){

        $filmListResult = mysqli_fetch_assoc($filmListObject);

        if($filmListResult["title"] == null){
            continue;
        }

        $filmID = $filmListResult["id"];
        $filmsOrder = $filmListResult["films_order"];

        $sessionDurationHours = (int)($filmListResult["session_duration"] / 60 / 60);
        $sessionDurationMinutes = (int)(($filmListResult["session_duration"] / 60) % 60);

        if($sessionDurationHours == 0){

            $sessionDurationResultString = "Продолжительность сеанса: <b>".$sessionDurationMinutes." мин.</b><br />";

        }elseif($sessionDurationMinutes == 0){
            // Не может быть случая 0 ч. 0 мин.
            $sessionDurationResultString = "Продолжительность сеанса: <b>".$sessionDurationHours." ч. ровно</b><br />";

        }else{

            $sessionDurationResultString = "Продолжительность сеанса: <b>".$sessionDurationHours." ч. ".$sessionDurationMinutes." мин.</b><br />";

        }

        $formatOut = "";
        $formatOutWiki = "";
        if($filmListResult["has_3d"] == 1){
            $formatOut = "Также и в 3D 👓";
            $formatOutWiki = "";
        } else {
            $formatOut = "Только 2D";
            $formatOutWiki = " | только 2D";
        }

        $premierOut = "";
        $premierOutWiki = "";
        if($filmListResult["is_premier"] == 1){
            $premierOut = " | премьера";
            $premierOutWiki = " | премьера";
        }

        $adsDuration = 313;
        $adsDurationMinutes = (int)($adsDuration / 60);
        $adsDurationSeconds = (int)($adsDuration % 60);

        $trailersDurationMinutes = (int)($filmListResult["trailers_duration"] / 60);
        $trailersDurationSeconds = (int)($filmListResult["trailers_duration"] % 60);

        if($trailersDurationMinutes == 0){

            if($trailersDurationSeconds == 0) {

                $trailersDurationResultString = "Продолжительность блока трейлеров: <b>трейлеры не ожидаются</b><br /><br />";

            } else{

                $trailersDurationResultString = "Продолжительность блока трейлеров: <b>".$trailersDurationSeconds." сек.</b><br /><br />";

            }

        }else{

            if($trailersDurationSeconds == 0) {

                $trailersDurationResultString = "Продолжительность блока трейлеров: <b>".$trailersDurationMinutes." мин. ровно</b><br /><br />";

            } else{

                $trailersDurationResultString = "Продолжительность блока трейлеров: <b>".$trailersDurationMinutes." мин. ".$trailersDurationSeconds." сек.</b><br /><br />";

            }

        }

        // BEGIN: Сам вывод информации
        echo("<div class=\"cards\">\n");

        echo("<div class=\"pre-wrap\"><h3>«".$filmListResult["title"]."» | ".$filmListResult["rating"]."+ ".$premierOut."</h3></div>");
        echo("Жанр: <b>".$filmListResult["genre"]."</b><br />");

        echo($sessionDurationResultString);
        echo("<form action=\"rqleve4.php\" method=\"post\">\n");
        echo("<input type=\"text\" name=\"sessionDurationHours\" size=\"2\" maxlength=\"2\" placeholder=\"00\" />\n");
        echo("<input type=\"text\" name=\"sessionDurationMinutes\" size=\"2\" maxlength=\"2\" placeholder=\"00\" />\n");
        echo("<button name=\"main\" value=\"filmList\">Изменить продолжительность сеанса</button><br /><br />\n");
        echo("<input type=\"hidden\" name=\"filmID\" value=\"".$filmID."\" />\n");
        echo("<input type=\"hidden\" name=\"todo\" value=\"changeSessionDuration\" />\n");
        echo("</form>\n");

        // echo("Продолжительность блока с рекламой и социальными роликами: <b>".$adsDurationMinutes." мин. ".$adsDurationSeconds." сек.</b><br />");
        // echo($trailersDurationResultString);

        echo("<b>СЮЖЕТ:</b><br /><div class=\"pre-wrap\">".html_entity_decode($filmListResult["plot"])."</div><br />");

        echo("<b>В РОЛЯХ:</b><br />".$filmListResult["cast"]."<br /><br />");

        echo("<b>СЛОГАН:</b><br />".$filmListResult["slogan"]."<br /><br />");

        echo("<b>Фильм на КиноПоиске:</b><br />");
        echo("<a href=\"".$filmListResult["kinopoisk_link"]."\" target=\"_blank\">".$filmListResult["kinopoisk_link"]."</a><br />");
        echo("<b>Фильм на КиноПлане:</b><br />");
        echo("<a href=\"".$filmListResult["kinoplan_link"]."\" target=\"_blank\">".$filmListResult["kinoplan_link"]."</a><br /><br />");
        echo("<b>Подробнее о фильме:</b><br />");
        echo("<a href=\"https://vk.com/@cinemabrooklyn-".$filmListResult["articles_link_end"]."\" target=\"_blank\">https://vk.com/@cinemabrooklyn-".$filmListResult["articles_link_end"]."</a><br /><br />");

        echo("—————————————————<br />");
        echo("<b>Пост с описанием в Телеграм:</b><br />");
        echo("<a href=\"".$filmListResult["tg_link"]."\" target=\"_blank\">".$filmListResult["tg_link"]."</a><br /><br />");
        echo("<form action=\"rqleve4.php\" method=\"post\">\n");
        echo("<input type=\"text\" name=\"tgLinkChange\" size=\"5\" maxlength=\"5\" placeholder=\"tgCh\" />\n");
        echo("<button name=\"main\" value=\"filmList\">Изменить номер поста описания в Телеграм</button><br /><br />\n");
        echo("<input type=\"hidden\" name=\"filmID\" value=\"".$filmID."\" />\n");
        echo("<input type=\"hidden\" name=\"todo\" value=\"changeTGLink\" />\n");
        echo("</form>\n");
        echo("—————————————————<br /><br />");

        echo("Дата релиза: <b>".$filmListResult["release_date"]."</b><br /><br />");

        echo("Дистрибьютор:<br />");
        echo("<b>".$filmListResult["distributor"]."</b>");
        echo("<br />");
        echo("<br />");

        if($filmsOrder != null){
            echo("Фильм № ".$filmsOrder);
        }

        echo("<form action=\"rqleve4.php\" method=\"post\">\n");
        echo("<input type=\"text\" name=\"filmsorder\" size=\"3\" maxlength=\"3\" placeholder=\"0\" />\n");
        echo("<button name=\"main\" value=\"filmList\">Изменить номер в очереди</button><br /><br />\n");
        echo("<input type=\"hidden\" name=\"filmID\" value=\"".$filmID."\" />\n");
        echo("<input type=\"hidden\" name=\"todo\" value=\"changefilmsorder\" />\n");
        echo("</form>\n");

        echo("<form action=\"rqleve4.php\" method=\"post\">\n");
        echo("<button name=\"main\" value=\"filmList\">Ой, это премьера!</button>\n");
        echo("<input type=\"hidden\" name=\"filmID\" value=\"".$filmID."\" />\n");
        echo("<input type=\"hidden\" name=\"todo\" value=\"setpremier\" />\n");
        echo("</form>\n");

        echo("<form action=\"rqleve4.php\" method=\"post\">\n");
        echo("<button name=\"main\" value=\"filmList\">Это уже не премьера...</button><br /><br />\n");
        echo("<input type=\"hidden\" name=\"filmID\" value=\"".$filmID."\" />\n");
        echo("<input type=\"hidden\" name=\"todo\" value=\"setnotpremier\" />\n");
        echo("</form>\n");

        echo("—————————————————");
        echo("<form action=\"rqleve4.php\" method=\"post\">\n");
        echo("<b>Создание краткого описания данной киноленты</b><br />");
        echo("<button name=\"main\" value=\"filmList\">Создать краткое описание</button><br />\n");
        echo("<input type=\"hidden\" name=\"filmID\" value=\"".$filmID."\" />\n");
        echo("<input type=\"hidden\" name=\"todo\" value=\"createShortCaption\" />");
        echo("</form>\n");
        echo("—————————————————");
        echo("<br />");

        echo("—————————————————");
        echo("<form action=\"rqleve4.php\" method=\"post\">\n");
        echo("<b>Создание среднедлинного описания данной киноленты</b><br />");
        echo("<button name=\"main\" value=\"filmList\">Создать среднедлинное описание</button><br />\n");
        echo("<input type=\"hidden\" name=\"filmID\" value=\"".$filmID."\" />\n");
        echo("<input type=\"hidden\" name=\"todo\" value=\"createMediumCaption\" />");
        echo("</form>\n");
        echo("—————————————————");
        echo("<br />");

        echo("<form class=\"rmv\" action=\"rqleve4.php\" method=\"post\">\n");
        echo("<button name=\"main\" value=\"filmList\">Удалить этот продукт</button><br />\n");
        echo("<input type=\"hidden\" name=\"filmID\" value=\"".$filmID."\" />\n");
        echo("<input type=\"hidden\" name=\"todo\" value=\"removefilm\" />\n");
        echo("</form>\n");

        echo("</div>");
        echo("<br />");
        // END: Сам вывод информации

        /*
        // Устаревший участок кода, но убирать пока не будем!
        // BEGIN: Создание файла с блоком этого фильма для вёрстки расписания в ВК
        $wikiELA = "|-\n";
        $wikiELA .= "| <center>[[photo-3324177_457245935|500px;nopadding| ]]</center> \n";
        $wikiELA .= "|-\n";
        $wikiELA .= "! [".$filmListResult["kinopoisk_link"]."|🔸 «".$filmListResult["title"]."»] <sup> <gray>".$filmListResult["rating"]."+".$formatOutWiki.$premierOutWiki."</gray> </sup>\n";
        $wikiELA .= "|-\n";
        $wikiELA .= "|<br/>\n";
        $wikiELA .= "|-\n";
        $wikiELA .= "|\n";
        $wikiELA .= "<gray><b>\n";
        for($i = 0; $i < 10; $i++){
            $wikiELA .= "• 00:00 | зал Premier Hall<br/>\n";
            $wikiELA .= "• 00:00 | зал Red Hall<br/>\n";
            $wikiELA .= "• 00:00 | зал Classic Hall<br/>\n";
            $wikiELA .= "• 00:00 | зал Green Hall<br/>\n";
        }
        $wikiELA .= "<b><gray>\n";
        $wikiELA .= "<br/>Продолжительность сеанса: '''".$sessionDurationHours." ч. ".$sessionDurationMiutes." мин.'''\n";
        $wikiELA .= "<br/>Жанр: '''".$filmListResult["genre"]."'''\n";
        $wikiELA .= "<br/>\n";
        $wikiELA .= "<br/>Продолжительность блока с рекламой: '''".$adsDurationMiutes." мин. ".$adsDurationSeconds." сек.'''\n";
        $wikiELA .= "<br/>Продолжительность блока с трейлерами: '''".$trailersDurationMiutes." мин. ".$trailersDurationSeconds." сек.'''\n";
        $wikiELA .= "|-\n";
        $wikiELA .= "| <center>[[photo-3324177_457245935|500px;nopadding| ]]</center> \n";

        $wikiZEL = "|-\n";
        $wikiZEL .= "| <center>[[photo-44334955_457247891|500px;nopadding| ]]</center> \n";
        $wikiZEL .= "|-\n";
        $wikiZEL .= "! [".$filmListResult["kinopoisk_link"]."|🔸 «".$filmListResult["title"]."»] <sup> <gray>".$filmListResult["rating"]."+".$formatOutWiki.$premierOutWiki."</gray> </sup>\n";
        $wikiZEL .= "|-\n";
        $wikiZEL .= "|<br/>\n";
        $wikiZEL .= "|-\n";
        $wikiZEL .= "|\n";
        $wikiZEL .= "<gray><b>\n";
        for($i = 0; $i < 10; $i++){
            $wikiZEL .= "• 00:00 | зал 1 • Красный<br/>\n";
            $wikiZEL .= "• 00:00 | зал 2 • Зелёный<br/>\n";
            $wikiZEL .= "• 00:00 | зал 3 • Синий<br/>\n";
        }
        $wikiZEL .= "<b><gray>\n";
        $wikiZEL .= "<br/>Продолжительность сеанса: '''".$sessionDurationHours." ч. ".$sessionDurationMiutes." мин.'''\n";
        $wikiZEL .= "<br/>Жанр: '''".$filmListResult["genre"]."'''\n";
        $wikiZEL .= "<br/>\n";
        $wikiZEL .= "<br/>Продолжительность блока с рекламой: '''".$adsDurationMiutes." мин. ".$adsDurationSeconds." сек.'''\n";
        $wikiZEL .= "<br/>Продолжительность блока с трейлерами: '''".$trailersDurationMiutes." мин. ".$trailersDurationSeconds." сек.'''\n";
        $wikiZEL .= "|-\n";
        $wikiZEL .= "| <center>[[photo-44334955_457247891|500px;nopadding| ]]</center> \n";

        $charsReplace = ['<', '>', '\\', '|', '/', '\"', ':', '?', '*'];
        $clearFilename = str_replace($charsReplace, '', $filmListResult["title"]);
        $filenameWikiELA = "cardsWiki/".time()." ".$filmListResult["id"]." ".$clearFilename." Wiki ELA.btt";
        $filenameWikiZEL = "cardsWiki/".time()." ".$filmListResult["id"]." ".$clearFilename." Wiki ZEL.btt";

        $fd1 = fopen($filenameWikiELA, 'w') or die("ELA: Не удалось создать файл...");
        fwrite($fd1, $wikiELA);
        fclose($fd1);

        $fd2 = fopen($filenameWikiZEL, 'w') or die("ZEL: Не удалось создать файл...");
        fwrite($fd2, $wikiZEL);
        fclose($fd2);
        // END: Создание файла с блоком этого фильма для вёрстки расписания в ВК
        */

        // BEGIN: Карточки фильма для постов
        $separator = str_repeat("•", 36);

        $premierOutPosts = "";
        if($filmListResult["is_premier"] == 1){
            $premierOutPosts = " | премьера";
        }

        $cardPostCOM = mb_strtoupper($filmListResult["slogan"])."\n";
        $cardPostCOM .= "«".$filmListResult["title"]."» | ".$filmListResult["rating"]."+".$premierOutPosts."\n";
        $cardPostCOM .= "Жанр: ".$filmListResult["genre"]."\n";
        $cardPostCOM .= "Продолжительность сеанса: ".$sessionDurationHours." ч. ".$sessionDurationMinutes." мин.\n";
        $cardPostCOM .= $formatOut."\n\n";

        $cardPostCOM .= "Продолжительность блока с рекламой и социальными роликами: ".$adsDurationMinutes." мин. ".$adsDurationSeconds." сек.\n";
        $cardPostCOM .= "Продолжительность блока с трейлерами: ".$trailersDurationMinutes." мин. ".$trailersDurationSeconds." сек.\n\n";

        if($filmListResult["has_3d"] == 1){
            $cardPostCOM .= $separator."\n";
            $cardPostCOM .= "🎬 СЕАНСЫ 3D\n";
            $cardPostCOM .= "🎬 С XX ПО XX XXXXXXXXXXXXXXXXX\n";
            $cardPostCOM .= $separator."\n\n".$separator."\n\n";
            $cardPostCOM .= $separator."\n";
            $cardPostCOM .= "🎬 СЕАНСЫ 2D\n";
            $cardPostCOM .= "🎬 С XX ПО XX XXXXXXXXXXXXXXXXX\n";
            $cardPostCOM .= $separator."\n\n".$separator."\n\n";
        } else {
            $cardPostCOM .= $separator."\n";
            $cardPostCOM .= "🎬 СЕАНСЫ [только 2D]\n";
            $cardPostCOM .= "🎬 С XX ПО XX XXXXXXXXXXXXXXXXX\n";
            $cardPostCOM .= $separator."\n\n".$separator."\n\n";
        }

        $cardPostCOM .= "СЮЖЕТ:\n";
        $cardPostCOM .= html_entity_decode($filmListResult["plot"])."\n\n";

        $cardPostCOM .= "В РОЛЯХ:\n";
        $cardPostCOM .= $filmListResult["cast"]."\n\n";

        $cardPostELA = $cardPostCOM."Бронирование билетов\n";
        $cardPostELA .= "и подробности по телефону:\n";
        $cardPostELA .= "☎ 8 (85557) 4 66 11\n";
        $cardPostELA .= "Мы в TELEGRAM:\n";
        $cardPostELA .= "https://t.me/brooklyncinema\n";
        $cardPostELA .= "Заходи в нашу группу ВК:\n";
        $cardPostELA .= "https://vk.com/brooklynkino\n\n";

        $cardPostELA .= "Фильм на КиноПоиске:\n";
        $cardPostELA .= $filmListResult["kinopoisk_link"];

        $cardPostZEL = $cardPostCOM."Бронирование билетов\n";
        $cardPostZEL .= "и подробности по телефону:\n";
        $cardPostZEL .= "☎ 8 (84371) 6 03 60\n";
        $cardPostZEL .= "Мы в TELEGRAM:\n";
        $cardPostZEL .= "https://t.me/brooklynzd\n";
        $cardPostZEL .= "Заходи в нашу группу ВК:\n";
        $cardPostZEL .= "https://vk.com/brooklyn2\n\n";

        $cardPostZEL .= "Фильм на КиноПоиске:\n";
        $cardPostZEL .= $filmListResult["kinopoisk_link"];

        $charsReplace = ['<', '>', '\\', '|', '/', '\"', ':', '?', '*'];
        $clearFilename = str_replace($charsReplace, '', $filmListResult["title"]);
        $filenameCardPostELA = "cardsPosts/".time()." ".$filmListResult["id"]." ".$clearFilename." Card Post ELA.btt";
        $filenameCardPostZEL = "cardsPosts/".time()." ".$filmListResult["id"]." ".$clearFilename." Card Post ZEL.btt";

        /*
        $fd3 = fopen($filenameCardPostELA, 'w') or die("ELA: Не удалось создать файл...");
        fwrite($fd3, $cardPostELA);
        fclose($fd3);

        $fd4 = fopen($filenameCardPostZEL, 'w') or die("ZEL: Не удалось создать файл...");
        fwrite($fd4, $cardPostZEL);
        fclose($fd4);
        */
        // END: Карточки фильма для постов

        echo("<form action=\"rqleve4.php\" method=\"post\">");
        echo("  <button name=\"main\" value=\"\">Обратно на главную</button>");
        echo("</form>");
        echo("<br />");
        echo("<br />");
        /*
        echo("Наименование фильма:<br /><b>«".$filmListResult["title"]."»</b><br /><br />");
        echo("Возрастное ограничение: <b>".$filmListResult["rating"]."+</b><br /><br />");
        echo("Жанры: <b>".$filmListResult["genre"]."</b><br /><br />");
        echo("</div>");
        echo("<br /><br />");
        */

        /*
        $filmListResult = mysqli_fetch_assoc($filmListObject);
        foreach($filmListResult as $key => $filmec){
            echo($key." - ".$filmec."<br />");
        }
        echo("<br /><br />");
        */

    }

?>
