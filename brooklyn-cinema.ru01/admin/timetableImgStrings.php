<?php

    // Размеры полоски 2156x316
    $xFull = 2156;
    $yFull = 316;

    // Fonts
    $fontQuanelasBlack = "C:/ITProjects/BROOKLYN Cinema/brooklyn-cinema/brooklyn-cinema.ru01/fonts/qanelas-black.ttf";
    $fontQuanelasBlackItalic = "C:/ITProjects/BROOKLYN Cinema/brooklyn-cinema/brooklyn-cinema.ru01/fonts/qanelas-black-italic.ttf";
    $fontSongerCHeavy = "C:/ITProjects/BROOKLYN Cinema/brooklyn-cinema/brooklyn-cinema.ru01/fonts/songer_c_heavy.otf";
    $fontSongerCMedium = "C:/ITProjects/BROOKLYN Cinema/brooklyn-cinema/brooklyn-cinema.ru01/fonts/songer_c_medium.otf";
    $fontSongerCRegular = "C:/ITProjects/BROOKLYN Cinema/brooklyn-cinema/brooklyn-cinema.ru01/fonts/songer_c_reqular.otf";

    // Texts Sizes
    $textSizeTime = 44;
    $textSizeHall = 44;

    $image = imagecreatefrompng($imagePath);
    imagealphablending($image, false); // Выключение альфа-смешения
    imagesavealpha($image, true); // Установка альфа-флага - сохраняем прозрачность

    // Colors
    $color000000 = imagecolorallocate($image, 0, 0, 0);
    $colorFFFFFF = imagecolorallocate($image, 255, 255, 255);
    $colorFF2533 = imagecolorallocate($image, 255, 37, 51);
    $colorF0D000 = imagecolorallocate($image, 240, 208, 0);
    $colorForRating = $colorFFFFFF;
    if($ratingSpecCol == 1){
        if($departmentsID == 1){
            $colorForRating = $color000000;
        }elseif($departmentsID == 2){
            $colorForRating = $colorFF2533;
        }
    }

    // Header Section
    $textWidth01 = imagettfbbox(70, 0, $fontQuanelasBlackItalic, "«»")[2];
    $textWidth02 = imagettfbbox(90, 0, $fontQuanelasBlackItalic, $filmTitle)[2];
    $x01 = ($xFull - ($textWidth01 + $textWidth02)) / 2;

    imagettftext($image, 70, 0, $x01, 100, $colorFFFFFF, $fontQuanelasBlackItalic, "«");
    $x02 = $x01 + $textWidth01 / 2;
    imagettftext($image, 90, 0, $x02, 100, $colorFFFFFF, $fontQuanelasBlackItalic, $filmTitle);
    $x03 = $x02 + $textWidth02;
    imagettftext($image, 70, 0, $x03, 100, $colorFFFFFF, $fontQuanelasBlackItalic, "»");

    // Main Info Section
    $textWidth03 = imagettfbbox(44, 0, $fontQuanelasBlackItalic,
                    $format2D.$format3D.$pipeSeparator.$filmRatingString." | ПРОДОЛЖИТЕЛЬНОСТЬ: ".$sessionDurationString)[2];
    $textWidth04 = imagettfbbox(44, 0, $fontQuanelasBlackItalic, $format2D)[2];
    $textWidth05 = imagettfbbox(44, 0, $fontQuanelasBlackItalic, $format3D)[2];
    $textWidth06 = imagettfbbox(44, 0, $fontQuanelasBlackItalic, $pipeSeparator)[2];
    $textWidth07 = imagettfbbox(44, 0, $fontQuanelasBlackItalic, $filmRatingString)[2];
    $textWidth08 = imagettfbbox(44, 0, $fontQuanelasBlackItalic, " | ПРОДОЛЖИТЕЛЬНОСТЬ: ".$sessionDurationString)[2];
    $x04 = ($xFull - $textWidth03) / 2;

    imagettftext($image, 44, 0, $x04, 175, $colorFFFFFF, $fontQuanelasBlackItalic, $format2D);
    $x05 = $x04 + $textWidth04;
    imagettftext($image, 44, 0, $x05, 175, $colorF0D000, $fontQuanelasBlackItalic, $format3D);
    $x06 = $x05 + $textWidth05;
    imagettftext($image, 44, 0, $x06, 175, $colorFFFFFF, $fontQuanelasBlackItalic, $pipeSeparator);
    $x07 = $x06 + $textWidth06;
    imagettftext($image, 44, 0, $x07, 175, $colorForRating, $fontQuanelasBlackItalic, $filmRatingString);
    $x08 = $x07 + $textWidth07;
    imagettftext($image, 44, 0, $x08, 175, $colorFFFFFF, $fontQuanelasBlackItalic, " | ПРОДОЛЖИТЕЛЬНОСТЬ: ".$sessionDurationString);

    $timesFirstWord = "ВРЕМЯ";
    $hallsFirstWord = "ЗАЛ";

    // $textWidth09 = imagettfbbox($textSizeTime, 0, $fontQuanelasBlackItalic, "ВРЕМЯ")[2];
    // $textWidth10 = imagettfbbox($textSizeHall, 0, $fontQuanelasBlackItalic, "ЗАЛ")[2];

    $timesFirstWordWidth = imagettfbbox($textSizeTime, 0, $fontQuanelasBlackItalic, $timesFirstWord)[2];
    $hallsFirstWordWidth = imagettfbbox($textSizeHall, 0, $fontQuanelasBlackItalic, $hallsFirstWord)[2];
    $firstWordSectionWidth = max($timesFirstWordWidth, $hallsFirstWordWidth);
    $timetableSectionWidth = $firstWordSectionWidth;

    // Вычисление ширины блока с сеансами
    for($i = 0; $i < $sessionsAmount; $i++){

        $sessionInfoAssoc = mysqli_fetch_assoc($sessionInfoObject2);

        $timeBegin = mb_substr($sessionInfoAssoc["TIME_BEGIN"], 0, 5);
        $hallID = $sessionInfoAssoc["HALLS_ID"];

        $sql01 = "SELECT NUMBER, TITLE_EN FROM halls WHERE ID = '".$hallID."';";
        $hallInfo = (mysqli_fetch_assoc(mysqli_query($dbConnection, $sql01)));

        if($departmentsID == 1){
            $hallString = $hallInfo["TITLE_EN"];
        }else{
            $hallString = $hallInfo["NUMBER"];
        }

        $textWidth09 = imagettfbbox($textSizeTime, 0, $fontQuanelasBlackItalic, $timeBegin)[2];
        $textWidth10 = imagettfbbox($textSizeHall, 0, $fontQuanelasBlackItalic, $hallString)[2];

        $timetableSectionWidth += (20 + max($textWidth09, $textWidth10));

    }

    $timetableSectionFullMargin = $xFull - $timetableSectionWidth;

    $timetableSectionBegin = (int)($timetableSectionFullMargin / 2);

    if($timesFirstWordWidth >= $hallsFirstWordWidth){
        imagettftext($image, $textSizeTime, 0, $timetableSectionBegin, 240, $colorFFFFFF, $fontQuanelasBlackItalic, $timesFirstWord);
        imagettftext($image, $textSizeHall, 0, ($timetableSectionBegin + (($timesFirstWordWidth - $hallsFirstWordWidth) / 2)), 295, $colorFFFFFF, $fontQuanelasBlackItalic, $hallsFirstWord);
    }else{
        imagettftext($image, $textSizeHall, 0, $timetableSectionBegin, 295, $colorFFFFFF, $fontQuanelasBlackItalic, $hallsFirstWord);
        imagettftext($image, $textSizeTime, 0, ($timetableSectionBegin + (($hallsFirstWordWidth - $timesFirstWordWidth) / 2)), 240, $colorFFFFFF, $fontQuanelasBlackItalic, $timesFirstWord);
    }

    $x10 = $timetableSectionBegin + max($timesFirstWordWidth, $hallsFirstWordWidth);

    // Формирование блока с сеансами
    for($i = 0; $i < $sessionsAmount; $i++){

        $sessionInfoAssoc = mysqli_fetch_assoc($sessionInfoObject3);

        $timeBegin = mb_substr($sessionInfoAssoc["TIME_BEGIN"], 0, 5);
        $hallID = $sessionInfoAssoc["HALLS_ID"];
        $is3D = $sessionInfoAssoc["IS_3D"];

        $sql02 = "SELECT NUMBER, TITLE_EN FROM halls WHERE ID = '".$hallID."';";
        $hallInfo = (mysqli_fetch_assoc(mysqli_query($dbConnection, $sql02)));

        if($departmentsID == 1){
            $hallString = $hallInfo["TITLE_EN"];
        }else{
            $hallString = $hallInfo["NUMBER"];
        }

        if($is3D == 1){
            $timeSessionColor = $colorF0D000;
            $hallSessionColor = $colorF0D000;
        }else{
            $timeSessionColor = $colorFFFFFF;
            $hallSessionColor = $colorFFFFFF;
        }

        $textWidth11 = imagettfbbox($textSizeTime, 0, $fontQuanelasBlackItalic, $timeBegin)[2];
        $textWidth12 = imagettfbbox($textSizeHall, 0, $fontQuanelasBlackItalic, $hallString)[2];

        if($textWidth11 >= $textWidth12){
            imagettftext($image, $textSizeTime, 0, $x10, 240, $timeSessionColor, $fontQuanelasBlackItalic, $timeBegin);
            imagettftext($image, $textSizeHall, 0, ($x10 + (($textWidth11 - $textWidth12) / 2)), 295, $hallSessionColor, $fontQuanelasBlackItalic, $hallString);
        }else{
            imagettftext($image, $textSizeHall, 0, $x10, 295, $hallSessionColor, $fontQuanelasBlackItalic, $hallString);
            imagettftext($image, $textSizeTime, 0, ($x10 + (($textWidth12 - $textWidth11) / 2)), 240, $timeSessionColor, $fontQuanelasBlackItalic, $timeBegin);
        }

        $x10 += (20 + max($textWidth11, $textWidth12));

    }

    if($c < 10){
        $number = "0".$c;
    }else{
        $number = $c;
    }

    $fileNameImgString = "timetableImgStringsResults/".$abbreviation." ".$yearStart.$mounthStart.$dayStart.$number." TimetableImgString ".time().".png";

    imagepng($image, $fileNameImgString);

    imagedestroy($image);
?>