<?php
    define("DB_HOST", "localhost");
    define("DB_LOGIN", "root");
    define("DB_PASS", "rQLevE4");
    define("DB_TITLE", "brooklyncinema");

    $dbConnection = mysqli_connect(DB_HOST, DB_LOGIN, DB_PASS, DB_TITLE);
    $imagePath = "ELA.png";

    // Fonts
    $fontQuanelasBlack = "C:/ITProjects/BROOKLYN Cinema/brooklyn-cinema/brooklyn-cinema.ru01/fonts/qanelas-black.ttf";
    $fontQuanelasBlackItalic = "C:/ITProjects/BROOKLYN Cinema/brooklyn-cinema/brooklyn-cinema.ru01/fonts/qanelas-black-italic.ttf";
    $fontSongerCHeavy = "C:/ITProjects/BROOKLYN Cinema/brooklyn-cinema/brooklyn-cinema.ru01/fonts/songer_c_heavy.otf";
    $fontSongerCMedium = "C:/ITProjects/BROOKLYN Cinema/brooklyn-cinema/brooklyn-cinema.ru01/fonts/songer_c_medium.otf";
    $fontSongerCRegular = "C:/ITProjects/BROOKLYN Cinema/brooklyn-cinema/brooklyn-cinema.ru01/fonts/songer_c_reqular.otf";

    $sql01 = "SELECT FILMS_ID FROM timetables
                    WHERE DATES_ID = '51' AND DEPARTMENTS_ID = '1' GROUP BY FILMS_ID;";
    $filmIDObject = mysqli_query($dbConnection, $sql01);
    $filmsAmount = mysqli_num_rows($filmIDObject);

    $x60 = 60;
    $x883 = 883;

    $y407 = 407;
    $y1267 = 1267;
    $y1354 = 1354;

    $y2222 = 1358;

    for($i = 0; $i < $filmsAmount; $i++){

        $image = imagecreatefrompng($imagePath);

        // Colors
        $color000000 = imagecolorallocate($image, 0, 0, 0);
        $colorFFFFFF = imagecolorallocate($image, 255, 255, 255);
        $colorFF2533 = imagecolorallocate($image, 255, 37, 51);

        $text = "Artur Muzafarov";

        $filmID = mysqli_fetch_assoc($filmIDObject)["FILMS_ID"];

        $sql02 = "SELECT RATING, SESSION_DURATION, GENRE FROM FILMS WHERE ID = '".$filmID."';";
        $filmInfo = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql02));

        if($filmInfo["GENRE"] == null){
            continue;
        }

        $ratingString = $filmInfo["RATING"]."+ |";
        $genreString1 = "Жанр:";
        $genreString2 = $filmInfo["GENRE"];

        $sessionDurationAbsolute = $filmInfo["SESSION_DURATION"];
        $sessionDurationHours = (int)($sessionDurationAbsolute / 60 / 60);
        $sessionDurationMinutes = (int)(($sessionDurationAbsolute / 60) % 60);

        $sessionDurationString1 = "| Продолжительность сеанса:";
        if($sessionDurationMinutes != 0){
            $sessionDurationString2 = $sessionDurationHours." ч. ".$sessionDurationMinutes." мин.";
        }else{
            $sessionDurationString2 = $sessionDurationHours." ч. ровно";
        }

        echo("<b>Test ".$i."</b><br />");
        echo("<b>".$filmID."</b><br /><br />");

        $sql03 = "SELECT DATE_START, DATE_END FROM dates WHERE ID = '51';";
        $datesAssoc = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql03));
        $dateStart = $datesAssoc["DATE_START"];
        $dateEnd = $datesAssoc["DATE_END"];

        $dayStart = mb_substr($dateStart, 8, 2);
        $mounthStart = mb_substr($dateStart, 5, 2);
        $yearStart = mb_substr($dateStart, 0, 4);

        $dayEnd = mb_substr($dateEnd, 8, 2);
        $mounthEnd = mb_substr($dateEnd, 5, 2);
        $yearEnd = mb_substr($dateEnd, 0, 4);

        $mounthStartString = "";
        $mounthEndString = "";

        switch($mounthStart){
            case "01":
                $mounthStartString = "января";
                break;
            case "02":
                $mounthStartString = "февраля";
                break;
            case "03":
                $mounthStartString = "марта";
                break;
            case "04":
                $mounthStartString = "апреля";
                break;
            case "05":
                $mounthStartString = "мая";
                break;
            case "06":
                $mounthStartString = "июня";
                break;
            case "07":
                $mounthStartString = "июля";
                break;
            case "08":
                $mounthStartString = "августа";
                break;
            case "09":
                $mounthStartString = "сентября";
                break;
            case "10":
                $mounthStartString = "октября";
                break;
            case "11":
                $mounthStartString = "ноября";
                break;
            case "12":
                $mounthStartString = "декабря";
                break;
            default:
                $mounthStartString = "НЕОПРЕДЕЛЁННОГО_МЕСЯЦА";
                break;
        }

        switch($mounthEnd){
            case "01":
                $mounthEndString = "января";
                break;
            case "02":
                $mounthEndString = "февраля";
                break;
            case "03":
                $mounthEndString = "марта";
                break;
            case "04":
                $mounthEndString = "апреля";
                break;
            case "05":
                $mounthEndString = "мая";
                break;
            case "06":
                $mounthEndString = "июня";
                break;
            case "07":
                $mounthEndString = "июля";
                break;
            case "08":
                $mounthEndString = "августа";
                break;
            case "09":
                $mounthEndString = "сентября";
                break;
            case "10":
                $mounthEndString = "октября";
                break;
            case "11":
                $mounthEndString = "ноября";
                break;
            case "12":
                $mounthEndString = "декабря";
                break;
            default:
                $mounthEndString = "НЕОПРЕДЕЛЁННОГО_МЕСЯЦА";
                break;
        }

        if($yearStart != $yearEnd){
            $period = "с ".$dayStart." ".$mounthStartString." ".$yearStart." года по ".$dayEnd." ".$mounthEndString." ".$yearEnd." года";
        } else{
            if($mounthStart != $mounthEnd){
                $period = "с ".$dayStart." ".$mounthStartString." по ".$dayEnd." ".$mounthEndString." ".$yearEnd." года";
            }else{
                if($dayStart != $dayEnd){
                    $period = "с ".$dayStart." по ".$dayEnd." ".$mounthStartString." ".$yearStart." года";
                }else{
                    $period = $dayStart." ".$mounthStartString." ".$yearStart." года";
                }
            }
        }

        // Блок с датами
        // 558x46 - сам блок, где пишется дата
        // x = 883, y = 407 - начало
        // Размер текста - в идеале 45
        $datesLenght = imagettfbbox(40, 0, $fontSongerCHeavy, $period)[2] - imagettfbbox(40, 0, $fontSongerCHeavy, $period)[0];
        $xVar04 = (558 - $datesLenght) / 2;

        if((558 - $datesLenght) >= 0){
            imagettftext($image, 40, 0, ($xVar04 + $x883), $y407, $colorFFFFFF, $fontSongerCHeavy, $period);
        }else{
            imagettftext($image, 40, 0, $x883, $y407, $colorFFFFFF, $fontSongerCHeavy, $period);
        }

        // Сеансы
        $sql04 = "SELECT TIME_BEGIN, TIME_END, IS_3D, HALLS_ID
                            FROM timetables WHERE FILMS_ID = '".$filmID."';";


        // Блок с технической информацией
        // 60x1267
        imagettftext($image, 45, 0, $x60, $y1267, $colorFFFFFF, $fontSongerCHeavy, $ratingString);
        $xVar01 = $x60 + imagettfbbox(45, 0, $fontSongerCHeavy, $ratingString)[2] + 3;

        imagettftext($image, 45, 0, $xVar01, $y1267, $colorFF2533, $fontSongerCHeavy, $sessionDurationString1);
        $xVar02 = $xVar01 + imagettfbbox(45, 0, $fontSongerCHeavy, $sessionDurationString1)[2] + 15;

        imagettftext($image, 45, 0, $xVar02, $y1267, $colorFFFFFF, $fontSongerCHeavy, $sessionDurationString2);

        imagettftext($image, 45, 0, $x60, $y1354, $colorFF2533, $fontSongerCHeavy, $genreString1);
        $xVar03 = $x60 + imagettfbbox(45, 0, $fontSongerCHeavy, $genreString1)[2] + 15;
        imagettftext($image, 45, 0, $xVar03, $y1354, $colorFFFFFF, $fontSongerCHeavy, $genreString2);
        // Сделать в БД короткий вариант жанра (если не влезает, будет выводить)

        // $y += 200;

        imagepng($image, "tmp/result".$i.".png");

        imagedestroy($image);

    }


    // imagettftext($image, 20, 0, 50, 50, $color, $fontPath, $text);

?>