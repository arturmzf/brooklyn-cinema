<?php

    // Точечный делитель
    $separator = str_repeat("•", 36);

    $resultTextView = "🎬🎞📽 Афиша кинотеатра BROOKLYN 🍿🥤\n";
    $resultTextView .= "\n";
    $resultTextView .= $separator."\n";
    $resultTextView .= "🎬 РАСПИСАНИЕ СЕАНСОВ\n";
    $resultTextView .= "🎬 ".mb_strtoupper($period)."\n";
    //$resultTextView .= strtoupper($period)."\n";
    $resultTextView .= $separator."\n";
    $resultTextView .= "🎫 Покупка билетов:\n";
    $resultTextView .= "https://супер8.рф/\n";
    // $resultTextView .= "📱 Если сайт на Вашем мобильном устройстве грузится долго, воспользуйтесь альтернативной ссылкой:\n";
    // $resultTextView .= "https://m.супер8.рф/\n";
    $resultTextView .= $separator."\n";

    $sql06 = "SELECT FILMS_ID FROM timetables WHERE DEPARTMENTS_ID = '".$departmentsID."' AND DATES_ID = '".$datesID."' GROUP BY FILMS_ID;";
    $resultFilmsListObject = mysqli_query($dbConnection, $sql06);
    $filmsAmount = mysqli_num_rows($resultFilmsListObject);

    for($i = 0; $i < $filmsAmount; $i++){

        $resultFilmsListAssoc = mysqli_fetch_assoc($resultFilmsListObject);

        $sql10 = "SELECT TITLE, HAS_3D FROM films WHERE ID = '".$resultFilmsListAssoc["FILMS_ID"]."';";
        $filmsInfo = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql10));

        if($filmsInfo["TITLE"] == null){
            continue;
        }

        if($filmsInfo["HAS_3D"] == 0){
            $sql07 = "SELECT TITLE, RATING, GENRE, SESSION_DURATION, TRAILERS_DURATION,
                            TG_LINK, IS_PREMIER, KINOPOISK_LINK, HAS_3D
                                    FROM films WHERE ID = '".$resultFilmsListAssoc["FILMS_ID"]."';";
            $aboutFilmResults = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql07));

            $isPremier = "";
            if($aboutFilmResults["IS_PREMIER"] == 1){
                $isPremier = " | премьера";
            }else{
                $isPremier = "";
            }

            $sessionDurationMinutesAbsolute = (int)($aboutFilmResults["SESSION_DURATION"] / 60);
            $sessionDurationHours = (int)($sessionDurationMinutesAbsolute / 60);
            $sessionDurationMinutes = $sessionDurationMinutesAbsolute % 60;

            if($sessionDurationMinutes == 0){
                $sessionDurationMinutesString = "ровно";
            }else{
                $sessionDurationMinutesString = $sessionDurationMinutes." мин.";
            }

            $adsDurationString = "";
            if($adsDuration != 0){
                $adsDurationMinutes = (int)($adsDuration / 60);
                $adsDurationSeconds = (int)($adsDuration % 60);

                if($adsDurationSeconds == 0){
                    $adsDurationString = "Продолжительность блока с рекламой и социальными роликами: ".$adsDurationMinutes." мин. ровно\n";
                }elseif($adsDurationMinutes == 0) {
                    $adsDurationString = "Продолжительность блока с рекламой и социальными роликами: ".$adsDurationSeconds." сек.\n";
                }else{
                    $adsDurationString = "Продолжительность блока с рекламой и социальными роликами: ".$adsDurationMinutes." мин. ".$adsDurationSeconds." сек.\n";
                }
            }else{
                $adsDurationString = "";
            }

            $trailersDuration = (int)($aboutFilmResults["TRAILERS_DURATION"]);
            $trailersDurationString = "";
            if($trailersDuration != 0){
                $trailersDurationMinutes = (int)($trailersDuration / 60);
                $trailersDurationSeconds = (int)($trailersDuration % 60);

                if($trailersDurationSeconds == 0){
                    $trailersDurationString = $trailersDurationMinutes." мин. ровно";
                }elseif($trailersDurationMinutes == 0){
                    $trailersDurationString = $trailersDurationSeconds." сек.";
                }else{
                    $trailersDurationString = $trailersDurationMinutes." мин. ".$trailersDurationSeconds." сек.";
                }
            }else{
                $trailersDurationString = "отсутствует";
            }

            $resultTextView .= "✳ «".$aboutFilmResults["TITLE"]."»\n";
            $resultTextView .= "ℹ ".$aboutFilmResults["RATING"]."+ | только 2D".$isPremier."\n";
            $resultTextView .= "\n";

            $sql13 = "SELECT TIME_BEGIN, TIME_END, HALLS_ID FROM timetables
                                WHERE FILMS_ID = '".$resultFilmsListAssoc["FILMS_ID"]."' AND 
                                DEPARTMENTS_ID = '".$departmentsID."' AND 
                                DATES_ID = '".$datesID."' AND IS_3D = '0' ORDER BY TIME_BEGIN;";
            $sessionsInfoObject = mysqli_query($dbConnection, $sql13);
            $sessionsAmount = mysqli_num_rows($sessionsInfoObject);

            for($count = 0; $count < $sessionsAmount; $count++){

                $sessionsInfoAssoc = mysqli_fetch_assoc($sessionsInfoObject);

                $timeSessionBegin = $sessionsInfoAssoc["TIME_BEGIN"];
                $timeSessionEnd = $sessionsInfoAssoc["TIME_END"];
                $sessionHallID = $sessionsInfoAssoc["HALLS_ID"];

                if((!empty($timeSessionBegin)) && (!empty($timeSessionEnd)) && (!empty($sessionHallID))){

                    $sql14 = "SELECT NUMBER, TITLE_EN, TITLE_RU FROM halls
                                                    WHERE ID = '".$sessionHallID."';";
                    $sessionHallInfo = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql14));

                    $hallNumber = $sessionHallInfo["NUMBER"];
                    $hallTitleEn = $sessionHallInfo["TITLE_EN"];
                    $hallTitleRu = $sessionHallInfo["TITLE_RU"];

                    if($departmentsID == 1){

                        $resultTextView .= "• ".mb_substr($timeSessionBegin, 0, 5)." | зал ".$hallTitleEn." Hall\n";

                    }else{

                        $resultTextView .= "• ".mb_substr($timeSessionBegin, 0, 5)." | зал ".$hallNumber." (".$hallTitleRu.")\n";

                    }

                }

            }

            $resultTextView .= "\n";
            $resultTextView .= "Продолжительность сеанса: ".$sessionDurationHours." ч. ".$sessionDurationMinutesString."\n";
            $resultTextView .= "Жанр: ".$aboutFilmResults["GENRE"]."\n\n";
            //$resultTextView .= "\n";
            //$resultTextView .= $adsDurationString;
            //$resultTextView .= "Продолжительность блока с трейлерами: ".$trailersDurationString."\n";
            $resultTextView .= "Подробнее о фильме ".$aboutFilmResults["TG_LINK"]."\n";
            $resultTextView .= $separator."\n";

        }else{

            $sql09 = "SELECT IS_3D FROM timetables WHERE FILMS_ID = '".$resultFilmsListAssoc["FILMS_ID"]."' GROUP BY IS_3D;";
            $formatSortingObject1 = mysqli_query($dbConnection, $sql09);
            $formatSortingCount1 = mysqli_num_rows($formatSortingObject1);

            for($c = 0; $c < $formatSortingCount1; $c++){

                $formatSortingAssoc = mysqli_fetch_assoc($formatSortingObject1);

                if($formatSortingAssoc["IS_3D"] == 0){

                    $sql11 = "SELECT TITLE, RATING, GENRE, SESSION_DURATION, TRAILERS_DURATION, IS_PREMIER, KINOPOISK_LINK, HAS_3D
                                    FROM films WHERE ID = '".$resultFilmsListAssoc["FILMS_ID"]."';";
                    $aboutFilmResults = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql11));

                    $isPremier = "";
                    if($aboutFilmResults["IS_PREMIER"] == 1){
                        $isPremier = " | премьера";
                    }else{
                        $isPremier = "";
                    }

                    $sessionDurationMinutesAbsolute = (int)($aboutFilmResults["SESSION_DURATION"] / 60);
                    $sessionDurationHours = (int)($sessionDurationMinutesAbsolute / 60);
                    $sessionDurationMinutes = $sessionDurationMinutesAbsolute % 60;

                    if($sessionDurationMinutes == 0){
                        $sessionDurationMinutesString = "ровно";
                    }else{
                        $sessionDurationMinutesString = $sessionDurationMinutes." мин.";
                    }

                    $adsDurationString = "";
                    if($adsDuration != 0){
                        $adsDurationMinutes = (int)($adsDuration / 60);
                        $adsDurationSeconds = (int)($adsDuration % 60);

                        if($adsDurationSeconds == 0){
                            $adsDurationString = "Продолжительность блока с рекламой и социальными роликами: ".$adsDurationMinutes." мин. ровно\n";
                        }elseif($adsDurationMinutes == 0) {
                            $adsDurationString = "Продолжительность блока с рекламой и социальными роликами: ".$adsDurationSeconds." сек.\n";
                        }else{
                            $adsDurationString = "Продолжительность блока с рекламой и социальными роликами: ".$adsDurationMinutes." мин. ".$adsDurationSeconds." сек.\n";
                        }
                    }else{
                        $adsDurationString = "";
                    }

                    $trailersDuration = (int)($aboutFilmResults["TRAILERS_DURATION"]);
                    $trailersDurationString = "";
                    if($trailersDuration != 0){
                        $trailersDurationMinutes = (int)($trailersDuration / 60);
                        $trailersDurationSeconds = (int)($trailersDuration % 60);

                        if($trailersDurationSeconds == 0){
                            $trailersDurationString = $trailersDurationMinutes." мин. ровно";
                        }elseif($trailersDurationMinutes == 0){
                            $trailersDurationString = $trailersDurationSeconds." сек.";
                        }else{
                            $trailersDurationString = $trailersDurationMinutes." мин. ".$trailersDurationSeconds." сек.";
                        }
                    }else{
                        $trailersDurationString = "отсутствует";
                    }

                    $resultTextView .= "✳ «".$aboutFilmResults["TITLE"]."» 2D\n";
                    $resultTextView .= "ℹ ".$aboutFilmResults["RATING"]."+".$isPremier."\n";
                    $resultTextView .= "\n";

                    $sql15 = "SELECT TIME_BEGIN, TIME_END, HALLS_ID FROM timetables
                                WHERE FILMS_ID = '".$resultFilmsListAssoc["FILMS_ID"]."' AND 
                                DEPARTMENTS_ID = '".$departmentsID."' AND 
                                DATES_ID = '".$datesID."' AND IS_3D = '0' ORDER BY TIME_BEGIN;";
                    $sessionsInfoObject = mysqli_query($dbConnection, $sql15);
                    $sessionsAmount = mysqli_num_rows($sessionsInfoObject);

                    for($count = 0; $count < $sessionsAmount; $count++){

                        $sessionsInfoAssoc = mysqli_fetch_assoc($sessionsInfoObject);

                        $timeSessionBegin = $sessionsInfoAssoc["TIME_BEGIN"];
                        $timeSessionEnd = $sessionsInfoAssoc["TIME_END"];
                        $sessionHallID = $sessionsInfoAssoc["HALLS_ID"];

                        if((!empty($timeSessionBegin)) && (!empty($timeSessionEnd)) && (!empty($sessionHallID))){

                            $sql17 = "SELECT NUMBER, TITLE_EN, TITLE_RU FROM halls
                                                    WHERE ID = '".$sessionHallID."';";
                            $sessionHallInfo = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql17));

                            $hallNumber = $sessionHallInfo["NUMBER"];
                            $hallTitleEn = $sessionHallInfo["TITLE_EN"];
                            $hallTitleRu = $sessionHallInfo["TITLE_RU"];

                            if($departmentsID == 1){

                                $resultTextView .= "• ".mb_substr($timeSessionBegin, 0, 5)." | зал ".$hallTitleEn." Hall<br/>\n";

                            }else{

                                $resultTextView .= "• ".mb_substr($timeSessionBegin, 0, 5)." | зал ".$hallNumber." • ".$hallTitleRu."<br/>\n";

                            }

                        }

                    }

                    $resultTextView .= "\n";
                    $resultTextView .= "Продолжительность сеанса: ".$sessionDurationHours." ч. ".$sessionDurationMinutesString."\n";
                    $resultTextView .= "Жанр: ".$aboutFilmResults["GENRE"]."\n";
                    //$resultTextView .= "\n";
                    //$resultTextView .= $adsDurationString;
                    //$resultTextView .= "Продолжительность блока с трейлерами: ".$trailersDurationString."\n";
                    $resultTextView .= $separator."\n";

                }else{

                    $sql12 = "SELECT TITLE, RATING, GENRE, SESSION_DURATION, TRAILERS_DURATION, IS_PREMIER, KINOPOISK_LINK, HAS_3D
                                    FROM films WHERE ID = '".$resultFilmsListAssoc["FILMS_ID"]."';";
                    $aboutFilmResults = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql12));

                    $isPremier = "";
                    if($aboutFilmResults["IS_PREMIER"] == 1){
                        $isPremier = " | премьера";
                    }else{
                        $isPremier = "";
                    }

                    $sessionDurationMinutesAbsolute = (int)($aboutFilmResults["SESSION_DURATION"] / 60);
                    $sessionDurationHours = (int)($sessionDurationMinutesAbsolute / 60);
                    $sessionDurationMinutes = $sessionDurationMinutesAbsolute % 60;

                    if($sessionDurationMinutes == 0){
                        $sessionDurationMinutesString = "ровно";
                    }else{
                        $sessionDurationMinutesString = $sessionDurationMinutes." мин.";
                    }

                    $adsDurationString = "";
                    if($adsDuration != 0){
                        $adsDurationMinutes = (int)($adsDuration / 60);
                        $adsDurationSeconds = (int)($adsDuration % 60);

                        if($adsDurationSeconds == 0){
                            $adsDurationString = "Продолжительность блока с рекламой и социальными роликами: ".$adsDurationMinutes." мин. ровно\n";
                        }elseif($adsDurationMinutes == 0) {
                            $adsDurationString = "Продолжительность блока с рекламой и социальными роликами: ".$adsDurationSeconds." сек.\n";
                        }else{
                            $adsDurationString = "Продолжительность блока с рекламой и социальными роликами: ".$adsDurationMinutes." мин. ".$adsDurationSeconds." сек.\n";
                        }
                    }else{
                        $adsDurationString = "";
                    }

                    $trailersDuration = (int)($aboutFilmResults["TRAILERS_DURATION"]);
                    $trailersDurationString = "";
                    if($trailersDuration != 0){
                        $trailersDurationMinutes = (int)($trailersDuration / 60);
                        $trailersDurationSeconds = (int)($trailersDuration % 60);

                        if($trailersDurationSeconds == 0){
                            $trailersDurationString = $trailersDurationMinutes." мин. ровно";
                        }elseif($trailersDurationMinutes == 0){
                            $trailersDurationString = $trailersDurationSeconds." сек.";
                        }else{
                            $trailersDurationString = $trailersDurationMinutes." мин. ".$trailersDurationSeconds." сек.";
                        }
                    }else{
                        $trailersDurationString = "отсутствует";
                    }

                    $resultTextView .= "✳ «".$aboutFilmResults["TITLE"]."» 3D\n";
                    $resultTextView .= "ℹ ".$aboutFilmResults["RATING"]."+".$isPremier."\n";
                    $resultTextView .= "\n";

                    $sql16 = "SELECT TIME_BEGIN, TIME_END, HALLS_ID FROM timetables
                                WHERE FILMS_ID = '".$resultFilmsListAssoc["FILMS_ID"]."' AND 
                                DEPARTMENTS_ID = '".$departmentsID."' AND 
                                DATES_ID = '".$datesID."' AND IS_3D = '1' ORDER BY TIME_BEGIN;";
                    $sessionsInfoObject = mysqli_query($dbConnection, $sql16);
                    $sessionsAmount = mysqli_num_rows($sessionsInfoObject);

                    for($count = 0; $count < $sessionsAmount; $count++){

                        $sessionsInfoAssoc = mysqli_fetch_assoc($sessionsInfoObject);

                        $timeSessionBegin = $sessionsInfoAssoc["TIME_BEGIN"];
                        $timeSessionEnd = $sessionsInfoAssoc["TIME_END"];
                        $sessionHallID = $sessionsInfoAssoc["HALLS_ID"];

                        if((!empty($timeSessionBegin)) && (!empty($timeSessionEnd)) && (!empty($sessionHallID))){

                            $sql18 = "SELECT NUMBER, TITLE_EN, TITLE_RU FROM halls
                                                    WHERE ID = '".$sessionHallID."';";
                            $sessionHallInfo = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql18));

                            $hallNumber = $sessionHallInfo["NUMBER"];
                            $hallTitleEn = $sessionHallInfo["TITLE_EN"];
                            $hallTitleRu = $sessionHallInfo["TITLE_RU"];

                            if($departmentsID == 1){

                                $resultTextView .= "• ".mb_substr($timeSessionBegin, 0, 5)." | зал ".$hallTitleEn." Hall<br/>\n";

                            }else{

                                $resultTextView .= "• ".mb_substr($timeSessionBegin, 0, 5)." | зал ".$hallNumber." • ".$hallTitleRu."<br/>\n";

                            }

                        }

                    }

                    $resultTextView .= "\n";
                    $resultTextView .= "Продолжительность сеанса: ".$sessionDurationHours." ч. ".$sessionDurationMinutesString."\n";
                    $resultTextView .= "Жанр: ".$aboutFilmResults["GENRE"]."\n";
                    //$resultTextView .= "\n";
                    //$resultTextView .= $adsDurationString;
                    //$resultTextView .= "Продолжительность блока с трейлерами: ".$trailersDurationString."\n";
                    $resultTextView .= $separator."\n";

                }

            }

        }

    }

    /*
    $charsReplace = ['<', '>', '\\', '|', '/', '\"', ':', '?', '*'];
    $clearFilename = str_replace($charsReplace, '', $filmListResult["title"]);
    $filenameWikiELA = "cardsWiki/".time()." ".$filmListResult["id"]." ".$clearFilename." Wiki ELA.btt";
    $filenameWikiZEL = "cardsWiki/".time()." ".$filmListResult["id"]." ".$clearFilename." Wiki ZEL.btt";
    */

    $resultTextView .= "🎫 Покупка билетов:\n";
    $resultTextView .= "https://супер8.рф/\n";
    // $resultTextView .= "📱 Если сайт на Вашем мобильном устройстве грузится долго, воспользуйтесь альтернативной ссылкой:\n";
    // $resultTextView .= "https://m.супер8.рф/\n";
    $resultTextView .= $separator."\n";

    $resultTextView .= "\n";

    if($departmentsID == 1){

        $resultTextView .= "Также доступно бронирование билетов\n";
        $resultTextView .= "и получение доп. информации по телефону:\n";
        $resultTextView .= "☎ 8 (85557) 4 66 11\n";
        // $resultTextView .= "Мы в TELEGRAM:\n";
        // $resultTextView .= "https://t.me/brooklyncinema\n";
        $resultTextView .= "Заходи в нашу группу VK:\n";
        $resultTextView .= "https://vk.com/brooklynkino";
    }else{
        $resultTextView .= "Также доступно бронирование билетов\n";
        $resultTextView .= "и получение доп. информации по телефону:\n";
        $resultTextView .= "☎ 8 (84371) 6 03 60\n";
        // $resultTextView .= "Мы в TELEGRAM:\n";
        // $resultTextView .= "https://t.me/brooklynzd\n";
        $resultTextView .= "Заходи в нашу группу VK:\n";
        $resultTextView .= "https://vk.com/brooklyn2";
    }


    $fileNameTextView = "timetableResults/".$abbreviation." ".$yearStart.$mounthStart.$dayStart." TG TimetableTextView ".time().".btt";

    $fdTextView = fopen($fileNameTextView, 'w') or die("Не удалось создать файл...");
    fwrite($fdTextView, $resultTextView);
    fclose($fdTextView);

?>