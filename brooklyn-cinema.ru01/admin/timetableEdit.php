<?php
    // Точечный делитель
    $separator = str_repeat("•", 36);

    echo("<h2>Перечень текущих фильмов</h2>");

    $sql01 = "SELECT ID, TITLE, HAS_3D FROM films ORDER BY films_order";
    $filmListObject = mysqli_query($dbConnection, $sql01);
    $filmListRows = mysqli_num_rows($filmListObject);

    for($i = 0; $i < $filmListRows; $i++){
        $filmListResult = mysqli_fetch_assoc($filmListObject);
        if($filmListResult["HAS_3D"] == 0){
            echo("<form action=\"rqleve4.php\" method=\"post\">");
            echo("<b>«".$filmListResult["TITLE"]."»</b>");
            echo("<br />");
            echo("Только 2D");
            echo("<br />");
            echo("<button name=\"main\" value=\"timetableEdit\">Добавить в расписание</button><br /><br />");
            echo("<input type=\"hidden\" name=\"todo\" value=\"addfilmtotimetable\" />");
            echo("<input type=\"hidden\" name=\"filmID\" value=\"".$filmListResult["ID"]."\" />");
            echo("<input type=\"hidden\" name=\"is3d\" value=\"0\" />");
            echo("<input type=\"hidden\" name=\"whatTimetable\" value=\"".$whatTimetable."\" />");
            echo("<input type=\"hidden\" name=\"datesID\" value=\"".$datesID."\" />");
            echo("<input type=\"hidden\" name=\"departmentsID\" value=\"".$departmentsID."\" />");
            echo("</form>");
        }else{
            echo("<form action=\"rqleve4.php\" method=\"post\">");
            echo("<b>«".$filmListResult["TITLE"]."»</b>");
            echo("<br />");
            echo("В 2D и 3D");
            echo("<br />");
            echo("<button name=\"main\" value=\"timetableEdit\">Добавить 2D в расписание</button>");
            echo("<input type=\"hidden\" name=\"todo\" value=\"addfilmtotimetable\" />");
            echo("<input type=\"hidden\" name=\"filmID\" value=\"".$filmListResult["ID"]."\" />");
            echo("<input type=\"hidden\" name=\"is3d\" value=\"0\" />");
            echo("<input type=\"hidden\" name=\"datesID\" value=\"".$datesID."\" />");
            echo("<input type=\"hidden\" name=\"departmentsID\" value=\"".$departmentsID."\" />");
            echo("</form>");
            echo("<form action=\"rqleve4.php\" method=\"post\">");
            echo("<button name=\"main\" value=\"timetableEdit\">Добавить 3D в расписание</button><br /><br />");
            echo("<input type=\"hidden\" name=\"todo\" value=\"addfilmtotimetable\" />");
            echo("<input type=\"hidden\" name=\"filmID\" value=\"".$filmListResult["ID"]."\" />");
            echo("<input type=\"hidden\" name=\"is3d\" value=\"1\" />");
            echo("<input type=\"hidden\" name=\"datesID\" value=\"".$datesID."\" />");
            echo("<input type=\"hidden\" name=\"departmentsID\" value=\"".$departmentsID."\" />");
            echo("</form>");
        }


    }

    echo("<br />");
    echo("<h2>Расписание</h2>");

    /*
    Нам понадобится список залов (в зависимости от города)
    BEGIN
    $sql05 = "SELECT NUMBER, TITLE_EN, TITLE_RU FROM halls WHERE DEPARTMENT_ID = '".$departmentsID."';";
    $hallsObject = mysqli_query($dbConnection, $sql05);
    $hallTitlesNum[mysqli_num_rows($hallsObject)] = null;
    $hallTitlesEn[mysqli_num_rows($hallsObject)] = null;
    $hallTitlesRu[mysqli_num_rows($hallsObject)] = null;

    $halls = 1;
    $i = 0;
    while($halls != null) {
        $halls = mysqli_fetch_assoc($hallsObject);
        $hallTitlesNum[$i] = $halls["NUMBER"];
        $hallTitlesEn[$i] = $halls["TITLE_EN"];
        $hallTitlesRu[$i] = $halls["TITLE_RU"];
        $i++;
    }
    // END
    */

    // $sql02 = "SELECT FILMS_ID, IS_3D FROM timetables WHERE DEPARTMENTS_ID = '".$departmentsID."' AND DATES_ID = '".$datesID."' GROUP BY FILMS_ID;";
    $sql02 = "SELECT FILMS_ID FROM timetables WHERE DEPARTMENTS_ID = '".$departmentsID."' AND 
                        DATES_ID = '".$datesID."' GROUP BY FILMS_ID;";
    $timetableObject = mysqli_query($dbConnection, $sql02);
    $timetableRows = mysqli_num_rows($timetableObject);

    for($j = 0; $j < $timetableRows; $j++){

        $timetable = mysqli_fetch_assoc($timetableObject);

        if(!empty($timetable["FILMS_ID"])){

            $sql03 = "SELECT TITLE, SESSION_DURATION FROM films WHERE ID = '".$timetable["FILMS_ID"]."';";

            $filmInfo = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql03));
            $filmTitle = $filmInfo["TITLE"];
            /*
            $formatString = "";
            if($timetable["IS_3D"] == 1){
                $formatString = " 3D";
            }else{
                $formatString = " 2D";
            }
            */
            $sessionDuration = $filmInfo["SESSION_DURATION"];

            $sql07 = "SELECT IS_3D FROM timetables WHERE FILMS_ID = '".$timetable["FILMS_ID"]."' GROUP BY IS_3D;";
            $formatSortingObject = mysqli_query($dbConnection, $sql07);
            $formatSortingCount = mysqli_num_rows($formatSortingObject);

            for($c = 0; $c < $formatSortingCount; $c++){

                $formatSortingAssoc = mysqli_fetch_assoc($formatSortingObject);

                if($formatSortingAssoc["IS_3D"] == 0){
                    echo("<div class=\"cards\">\n");
                    // echo("«".$filmTitle."» - ".$timetable["DEPARTMENTS_ID"]." - ".$timetable["DATES_ID"]);
                    echo("<h4>«".$filmTitle."» 2D</h4>\n");
                    echo("<form class=\"rmv\" action=\"rqleve4.php\" method=\"post\">");
                    echo("<button name=\"main\" value=\"timetableEdit\">Удалить этот фильм</button>\n");
                    echo("<input type=\"hidden\" name=\"todo\" value=\"rmvFilmFromTTB\" />");
                    echo("<input type=\"hidden\" name=\"whatTimetable\" value=\"".$whatTimetable."\" />");
                    echo("<input type=\"hidden\" name=\"datesID\" value=\"".$datesID."\" />");
                    echo("<input type=\"hidden\" name=\"filmID\" value=\"".$timetable["FILMS_ID"]."\" />");
                    echo("<input type=\"hidden\" name=\"departmentsID\" value=\"".$departmentsID."\" />");
                    echo("<input type=\"hidden\" name=\"is3d\" value=\"0\" />");
                    echo("</form>");
                    echo($separator);
                    echo("<br />\n");
                    echo("<br />\n");

                    // ДОБАВЬ TIME_END, как разберешься!!!
                    $sql04 = "SELECT ID, TIME_BEGIN, TIME_END, HALLS_ID FROM timetables
                                    WHERE FILMS_ID = '".$timetable["FILMS_ID"]."' 
                                    AND DEPARTMENTS_ID = '".$departmentsID."' 
                                    AND IS_3D = '0' 
                                    AND DATES_ID = '".$datesID."' ORDER BY TIME_BEGIN;";
                    $sessionsObject = mysqli_query($dbConnection, $sql04);
                    // $sessionsRows = mysqli_num_rows($sessionsObject);

                    // for($i = 0; $i < $sessionsRows; $i++){
                    $sessions = 1;
                    echo("<table>");
                    while($sessions != null){
                        $sessions = mysqli_fetch_assoc($sessionsObject);
                        $sessionID = $sessions["ID"];
                        // ДОБАВЬ TIME_END, как разберешься!!!
                        echo("<tr>");
                        if(($sessions["TIME_BEGIN"] != null) && ($sessions["HALLS_ID"] != null)){

                            $sql05 = "SELECT NUMBER, TITLE_EN FROM halls WHERE ID = '".$sessions["HALLS_ID"]."';";
                            $hallInfo = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql05));

                            // ДОБАВЬ TIME_END, как разберешься!!!
                            if($departmentsID == 1){
                                echo("<form class=\"rmv\" action=\"rqleve4.php\" method=\"post\">");

                                echo("<td><button name=\"main\" value=\"timetableEdit\">Удалить этот сеанс</button></td>\n");
                                echo("<td>•</td><td>".mb_substr($sessions["TIME_BEGIN"], 0, 5)."</td><td>—</td><td>".mb_substr($sessions["TIME_END"], 0, 5)."</td><td> | </td><td>".$hallInfo["TITLE_EN"]."\n</td><td> Hall</td>\n");
                                echo("<input type=\"hidden\" name=\"todo\" value=\"rmvsession\" />");
                                echo("<input type=\"hidden\" name=\"sessionID\" value=\"".$sessionID."\" />");
                                echo("<input type=\"hidden\" name=\"whatTimetable\" value=\"".$whatTimetable."\" />");
                                echo("<input type=\"hidden\" name=\"datesID\" value=\"".$datesID."\" />");
                                echo("<input type=\"hidden\" name=\"departmentsID\" value=\"".$departmentsID."\" />");
                                echo("</form>");
                            } else {
                                echo("<form class=\"rmv\" action=\"rqleve4.php\" method=\"post\">");
                                echo("<td><button name=\"main\" value=\"timetableEdit\">Удалить этот сеанс</button></td>\n");
                                echo("<td>•</td><td>".mb_substr($sessions["TIME_BEGIN"], 0, 5)."</td><td>—</td><td>".mb_substr($sessions["TIME_END"], 0, 5)."</td><td> | </td><td> зал </td><td>".$hallInfo["NUMBER"]."\n</td>\n");
                                echo("<input type=\"hidden\" name=\"todo\" value=\"rmvsession\" />");
                                echo("<input type=\"hidden\" name=\"sessionID\" value=\"".$sessionID."\" />");
                                echo("<input type=\"hidden\" name=\"whatTimetable\" value=\"".$whatTimetable."\" />");
                                echo("<input type=\"hidden\" name=\"datesID\" value=\"".$datesID."\" />");
                                echo("<input type=\"hidden\" name=\"departmentsID\" value=\"".$departmentsID."\" />");
                                echo("</form>");
                            }

                            /*
                            if($timetable["DEPARTMENTS_ID"] == 1){
                                echo("•".mb_substr($timetable["TIME_BEGIN"], 0, 5)." - ".mb_substr($timetable["TIME_END"], 0, 5)." | ".$hallInfo["TITLE_EN"]."<br /><br />");
                            } else {
                                echo("•".mb_substr($timetable["TIME_BEGIN"], 0, 5)." - ".mb_substr($timetable["TIME_END"], 0, 5)." | зал ".$hallInfo["NUMBER"]."<br /><br />");
                            }
                            */

                        }
                        echo("</tr>");
                    }
                    echo("</table>");

                    echo("<br />\n");
                    echo("<form action=\"rqleve4.php\" method=\"post\">\n");
                    echo("Время начала сеанса: ");
                    echo("<select name=\"sessionstarthour\" size=\"1\">\n");
                    echo("<option value=\"00\">00</option>\n");
                    echo("<option value=\"01\">01</option>\n");
                    echo("<option value=\"07\">07</option>\n");
                    echo("<option value=\"08\">08</option>\n");
                    echo("<option value=\"09\">09</option>\n");
                    echo("<option value=\"10\">10</option>\n");
                    echo("<option value=\"11\">11</option>\n");
                    echo("<option value=\"12\">12</option>\n");
                    echo("<option value=\"13\">13</option>\n");
                    echo("<option value=\"14\">14</option>\n");
                    echo("<option value=\"15\">15</option>\n");
                    echo("<option value=\"16\">16</option>\n");
                    echo("<option value=\"17\">17</option>\n");
                    echo("<option value=\"18\">18</option>\n");
                    echo("<option value=\"19\">19</option>\n");
                    echo("<option value=\"20\">20</option>\n");
                    echo("<option value=\"21\">21</option>\n");
                    echo("<option value=\"22\">22</option>\n");
                    echo("<option value=\"23\">23</option>\n");
                    echo("</select>\n");
                    echo(":");
                    echo("<select name=\"sessionstartminute\" size=\"1\">\n");
                    echo("<option value=\"00\">00</option>\n");
                    echo("<option value=\"05\">05</option>\n");
                    echo("<option value=\"10\">10</option>\n");
                    echo("<option value=\"15\">15</option>\n");
                    echo("<option value=\"20\">20</option>\n");
                    echo("<option value=\"25\">25</option>\n");
                    echo("<option value=\"30\">30</option>\n");
                    echo("<option value=\"35\">35</option>\n");
                    echo("<option value=\"40\">40</option>\n");
                    echo("<option value=\"45\">45</option>\n");
                    echo("<option value=\"50\">50</option>\n");
                    echo("<option value=\"55\">55</option>\n");
                    echo("</select>\n");
                    echo("<br />\n");
                    echo("Зал: ");
                    echo("<select name=\"sessionhall\" size=\"1\">\n");

                    $sql06 = "SELECT ID, NUMBER, TITLE_EN, TITLE_RU FROM halls WHERE DEPARTMENT_ID = '".$departmentsID."';";
                    $hallsObject = mysqli_query($dbConnection, $sql06);

                    for($i = 0; $i < mysqli_num_rows($hallsObject); $i++){
                        $halls = mysqli_fetch_assoc($hallsObject);

                        echo("<option value=\"".$halls["ID"]."\">".$halls["NUMBER"]." - ".$halls["TITLE_EN"]."</option>\n");

                    }

                    echo("</select>\n");
                    echo("<br />\n");
                    echo("<br />\n");
                    echo("<button name=\"main\" value=\"timetableEdit\">Добавить сеанс</button>");
                    echo("<input type=\"hidden\" name=\"todo\" value=\"addsession\" />");
                    echo("<input type=\"hidden\" name=\"is3d\" value=\"0\" />");
                    echo("<input type=\"hidden\" name=\"filmID\" value=\"".$timetable["FILMS_ID"]."\" />");
                    echo("<input type=\"hidden\" name=\"whatTimetable\" value=\"".$whatTimetable."\" />");
                    echo("<input type=\"hidden\" name=\"datesID\" value=\"".$datesID."\" />");
                    echo("<input type=\"hidden\" name=\"departmentsID\" value=\"".$departmentsID."\" />");
                    echo("<input type=\"hidden\" name=\"sessionduration\" value=\"".$sessionDuration."\" />");
                    echo("</form>\n");

                    // ЗАПОЛНЕНИЕ!
                    // echo();
                    echo("</div>\n");
                    echo("<br />\n");

                }else{

                    echo("<div class=\"cards\">\n");
                    // echo("«".$filmTitle."» - ".$timetable["DEPARTMENTS_ID"]." - ".$timetable["DATES_ID"]);
                    echo("<h4>«".$filmTitle."» 3D</h4>\n");
                    echo("<form class=\"rmv\" action=\"rqleve4.php\" method=\"post\">");
                    echo("<button name=\"main\" value=\"timetableEdit\">Удалить этот фильм</button>\n");
                    echo("<input type=\"hidden\" name=\"todo\" value=\"rmvFilmFromTTB\" />");
                    echo("<input type=\"hidden\" name=\"whatTimetable\" value=\"".$whatTimetable."\" />");
                    echo("<input type=\"hidden\" name=\"datesID\" value=\"".$datesID."\" />");
                    echo("<input type=\"hidden\" name=\"filmID\" value=\"".$timetable["FILMS_ID"]."\" />");
                    echo("<input type=\"hidden\" name=\"departmentsID\" value=\"".$departmentsID."\" />");
                    echo("<input type=\"hidden\" name=\"is3d\" value=\"1\" />");
                    echo("</form>");
                    echo($separator);
                    echo("<br />\n");
                    echo("<br />\n");

                    // ДОБАВЬ TIME_END, как разберешься!!!
                    $sql04 = "SELECT TIME_BEGIN, TIME_END, HALLS_ID FROM timetables
                                    WHERE FILMS_ID = '".$timetable["FILMS_ID"]."' AND 
                                    DEPARTMENTS_ID = '".$departmentsID."' AND IS_3D = '1' ORDER BY TIME_BEGIN;";
                    $sessionsObject = mysqli_query($dbConnection, $sql04);
                    // $sessionsRows = mysqli_num_rows($sessionsObject);

                    // for($i = 0; $i < $sessionsRows; $i++){
                    $sessions = 1;
                    while($sessions != null){
                        $sessions = mysqli_fetch_assoc($sessionsObject);

                        // ДОБАВЬ TIME_END, как разберешься!!!
                        if(($sessions["TIME_BEGIN"] != null) && ($sessions["HALLS_ID"] != null)){

                            $sql05 = "SELECT NUMBER, TITLE_EN FROM halls WHERE ID = '".$sessions["HALLS_ID"]."';";
                            $hallInfo = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql05));

                            // ДОБАВЬ TIME_END, как разберешься!!!
                            if($departmentsID == 1){
                                echo("<form class=\"rmv\" action=\"rqleve4.php\" method=\"post\">");
                                echo("<button name=\"main\" value=\"timetableEdit\">Удалить этот сеанс</button>\n");
                                echo("•".mb_substr($sessions["TIME_BEGIN"], 0, 5)." — ".mb_substr($sessions["TIME_END"], 0, 5)." | ".$hallInfo["TITLE_EN"]."\n<br />\n");
                                echo("<input type=\"hidden\" name=\"todo\" value=\"rmvsession\" />");
                                echo("<input type=\"hidden\" name=\"sessionID\" value=\"".$sessionID."\" />");
                                echo("<input type=\"hidden\" name=\"whatTimetable\" value=\"".$whatTimetable."\" />");
                                echo("<input type=\"hidden\" name=\"datesID\" value=\"".$datesID."\" />");
                                echo("<input type=\"hidden\" name=\"departmentsID\" value=\"".$departmentsID."\" />");
                                echo("</form>");
                            } else {
                                echo("<form class=\"rmv\" action=\"rqleve4.php\" method=\"post\">");
                                echo("<button name=\"main\" value=\"timetableEdit\">Удалить этот сеанс</button>\n");
                                echo("•".mb_substr($sessions["TIME_BEGIN"], 0, 5)." — ".mb_substr($sessions["TIME_END"], 0, 5)." | зал ".$hallInfo["NUMBER"]."\n<br />\n");
                                echo("<input type=\"hidden\" name=\"todo\" value=\"rmvsession\" />");
                                echo("<input type=\"hidden\" name=\"sessionID\" value=\"".$sessionID."\" />");
                                echo("<input type=\"hidden\" name=\"whatTimetable\" value=\"".$whatTimetable."\" />");
                                echo("<input type=\"hidden\" name=\"datesID\" value=\"".$datesID."\" />");
                                echo("<input type=\"hidden\" name=\"departmentsID\" value=\"".$departmentsID."\" />");
                                echo("</form>");
                            }

                            /*
                            if($timetable["DEPARTMENTS_ID"] == 1){
                                echo("•".mb_substr($timetable["TIME_BEGIN"], 0, 5)." - ".mb_substr($timetable["TIME_END"], 0, 5)." | ".$hallInfo["TITLE_EN"]."<br /><br />");
                            } else {
                                echo("•".mb_substr($timetable["TIME_BEGIN"], 0, 5)." - ".mb_substr($timetable["TIME_END"], 0, 5)." | зал ".$hallInfo["NUMBER"]."<br /><br />");
                            }
                            */

                        }

                    }

                    echo("<br />\n");
                    echo("<form action=\"rqleve4.php\" method=\"post\">\n");
                    echo("Время начала сеанса: ");
                    echo("<select name=\"sessionstarthour\" size=\"1\">\n");
                    echo("<option value=\"00\">00</option>\n");
                    echo("<option value=\"01\">01</option>\n");
                    echo("<option value=\"07\">07</option>\n");
                    echo("<option value=\"08\">08</option>\n");
                    echo("<option value=\"09\">09</option>\n");
                    echo("<option value=\"10\">10</option>\n");
                    echo("<option value=\"11\">11</option>\n");
                    echo("<option value=\"12\">12</option>\n");
                    echo("<option value=\"13\">13</option>\n");
                    echo("<option value=\"14\">14</option>\n");
                    echo("<option value=\"15\">15</option>\n");
                    echo("<option value=\"16\">16</option>\n");
                    echo("<option value=\"17\">17</option>\n");
                    echo("<option value=\"18\">18</option>\n");
                    echo("<option value=\"19\">19</option>\n");
                    echo("<option value=\"20\">20</option>\n");
                    echo("<option value=\"21\">21</option>\n");
                    echo("<option value=\"22\">22</option>\n");
                    echo("<option value=\"23\">23</option>\n");
                    echo("</select>\n");
                    echo(":");
                    echo("<select name=\"sessionstartminute\" size=\"1\">\n");
                    echo("<option value=\"00\">00</option>\n");
                    echo("<option value=\"05\">05</option>\n");
                    echo("<option value=\"10\">10</option>\n");
                    echo("<option value=\"15\">15</option>\n");
                    echo("<option value=\"20\">20</option>\n");
                    echo("<option value=\"25\">25</option>\n");
                    echo("<option value=\"30\">30</option>\n");
                    echo("<option value=\"35\">35</option>\n");
                    echo("<option value=\"40\">40</option>\n");
                    echo("<option value=\"45\">45</option>\n");
                    echo("<option value=\"50\">50</option>\n");
                    echo("<option value=\"55\">55</option>\n");
                    echo("</select>\n");
                    echo("<br />\n");
                    echo("Зал: ");
                    echo("<select name=\"sessionhall\" size=\"1\">\n");

                    $sql06 = "SELECT ID, NUMBER, TITLE_EN, TITLE_RU FROM halls WHERE DEPARTMENT_ID = '".$departmentsID."';";
                    $hallsObject = mysqli_query($dbConnection, $sql06);

                    for($i = 0; $i < mysqli_num_rows($hallsObject); $i++){
                        $halls = mysqli_fetch_assoc($hallsObject);

                        echo("<option value=\"".$halls["ID"]."\">".$halls["NUMBER"]." - ".$halls["TITLE_EN"]."</option>\n");

                    }

                    echo("</select>\n");
                    echo("<br />\n");
                    echo("<br />\n");
                    echo("<button name=\"main\" value=\"timetableEdit\">Добавить сеанс</button>");
                    echo("<input type=\"hidden\" name=\"todo\" value=\"addsession\" />");
                    echo("<input type=\"hidden\" name=\"is3d\" value=\"1\" />");
                    echo("<input type=\"hidden\" name=\"filmID\" value=\"".$timetable["FILMS_ID"]."\" />");
                    echo("<input type=\"hidden\" name=\"datesID\" value=\"".$datesID."\" />");
                    echo("<input type=\"hidden\" name=\"departmentsID\" value=\"".$departmentsID."\" />");
                    echo("<input type=\"hidden\" name=\"sessionduration\" value=\"".$sessionDuration."\" />");
                    echo("</form>\n");

                    // ЗАПОЛНЕНИЕ!
                    // echo();
                    echo("</div>\n");
                    echo("<br />\n");

                }


            }

        }


    }




    /*$sql02 = "SELECT * FROM timetables WHERE DEPARTMENTS_ID = '".$departmentsID."' AND DATES_ID = '".$datesID."';";
    $timetableObject = mysqli_query($dbConnection, $sql02);

    $timetable = 1;
    while($timetable != null){

        $timetable = mysqli_fetch_assoc($timetableObject);

        if(!empty($timetable["FILMS_ID"])){

            $sql03 = "SELECT TITLE FROM films WHERE ID = '".$timetable["FILMS_ID"]."';";
            $filmTitle = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql03))["TITLE"];

            echo("<div class=\"film-card\">\n");
            // echo("«".$filmTitle."» - ".$timetable["DEPARTMENTS_ID"]." - ".$timetable["DATES_ID"]);
            echo("<b>«".$filmTitle."»</b>");
            echo("<br />");

            if (!empty($timetable["TIME_BEGIN"]) && !empty($timetable["HALLS_ID"])){

                $sql04 = "SELECT NUMBER, TITLE_EN FROM halls WHERE ID = '".$timetable["HALLS_ID"]."';";
                $sessionInfo = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql04));

                if($timetable["DEPARTMENTS_ID"] == 1){
                    echo("•".mb_substr($timetable["TIME_BEGIN"], 0, 5)." - ".mb_substr($timetable["TIME_END"], 0, 5)." | ".$sessionInfo["TITLE_EN"]."<br /><br />");
                } else {
                    echo("•".mb_substr($timetable["TIME_BEGIN"], 0, 5)." - ".mb_substr($timetable["TIME_END"], 0, 5)." | ".$sessionInfo["NUMBER"]."<br /><br />");
                }

            }

            echo("<br />\n");
            echo("<form action=\"rqleve4.php\" method=\"post\">\n");
            echo("Время начала сеанса: ");
            echo("<select name=\"sessionstarthour\" size=\"1\">\n");
            echo("<option value=\"07\">07</option>\n");
            echo("<option value=\"08\">08</option>\n");
            echo("<option value=\"09\">09</option>\n");
            echo("<option value=\"10\">10</option>\n");
            echo("<option value=\"11\">11</option>\n");
            echo("<option value=\"12\">12</option>\n");
            echo("<option value=\"13\">13</option>\n");
            echo("<option value=\"14\">14</option>\n");
            echo("<option value=\"15\">15</option>\n");
            echo("<option value=\"16\">16</option>\n");
            echo("<option value=\"17\">17</option>\n");
            echo("<option value=\"18\">18</option>\n");
            echo("<option value=\"19\">19</option>\n");
            echo("<option value=\"20\">20</option>\n");
            echo("<option value=\"21\">21</option>\n");
            echo("<option value=\"22\">22</option>\n");
            echo("<option value=\"23\">23</option>\n");
            echo("</select>\n");
            echo(":");
            echo("<select name=\"sessionstartminute\" size=\"1\">\n");
            echo("<option value=\"00\">00</option>\n");
            echo("<option value=\"05\">05</option>\n");
            echo("<option value=\"10\">10</option>\n");
            echo("<option value=\"15\">15</option>\n");
            echo("<option value=\"20\">20</option>\n");
            echo("<option value=\"25\">25</option>\n");
            echo("<option value=\"30\">30</option>\n");
            echo("<option value=\"35\">35</option>\n");
            echo("<option value=\"40\">40</option>\n");
            echo("<option value=\"45\">45</option>\n");
            echo("<option value=\"50\">50</option>\n");
            echo("<option value=\"55\">55</option>\n");
            echo("</select>\n");
            echo("<br />\n");
            echo("Зал: ");
            echo("<select name=\"sessionhall\" size=\"1\">\n");

            $sql05 = "SELECT ID, NUMBER, TITLE_EN, TITLE_RU FROM halls WHERE DEPARTMENT_ID = '".$departmentsID."';";
            $hallsObject = mysqli_query($dbConnection, $sql05);

            for($i = 0; $i < mysqli_num_rows($hallsObject); $i++){
                $halls = mysqli_fetch_assoc($hallsObject);

                echo("<option value=\"".$halls["ID"]."\">".$halls["NUMBER"]." - ".$halls["TITLE_EN"]."</option>\n");

            }

            echo("</select>\n");
            echo("<br />\n");
            echo("<br />\n");
            echo("<button name=\"main\" value=\"timetableEdit\">Добавить сеанс</button>");
            echo("<input type=\"hidden\" name=\"addsession\" value=\"1\" />");
            echo("<input type=\"hidden\" name=\"filmID\" value=\"".$timetable["FILMS_ID"]."\" />");
            echo("<input type=\"hidden\" name=\"datesID\" value=\"".$datesID."\" />");
            echo("<input type=\"hidden\" name=\"departmentsID\" value=\"".$departmentsID."\" />");
            echo("</form>\n");

            // ЗАПОЛНЕНИЕ!
            // echo();
            echo("</div>\n");
            echo("<br />\n");
        }

    }*/












    echo("<br />");
    echo("<form action=\"rqleve4.php\" method=\"post\">");
    echo("  <button name=\"main\" value=\"\">Обратно на главную</button>");
    echo("</form>");
    echo("<br />");

?>