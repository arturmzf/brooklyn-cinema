<?php

    // Точечный делитель
    $separator = str_repeat("•", 36);

    $shortFilmCaption = "";

    $sql01 = "SELECT title, rating, genre,
                has_3d, plot, cast, distributor,
                articles_link_end FROM films
                    WHERE id = '".$filmID."';";
    $filmAboutObject = mysqli_query($dbConnection, $sql01);
    $filmAbout = mysqli_fetch_assoc($filmAboutObject);

    $filmTitle = $filmAbout["title"];
    $filmRating = $filmAbout["rating"];
    $filmGenre = $filmAbout["genre"];

    $has3D = $filmAbout["has_3d"];
    $about3D = "";
    if($has3D == 1){
        $about3D = "В 3D и 2D";
    }elseif($has3D == 0){
        $about3D = "Только 2D";
    }else{
        $about3D = "Error";
    }

    $filmPlot = $filmAbout["plot"];
    $filmCast = $filmAbout["cast"];

    if($filmAbout["distributor"] == "—"){
        $filmDistributor = "";
    }else{
        $filmDistributor = "\n\nДистрибьютор:\n".$filmAbout["distributor"];
    }

    $articlesLinkEnd = $filmAbout["articles_link_end"];

    // Запись в переменную для дальнейшей записи в файл
    $shortFilmCaption = "«".$filmTitle."» | ".$filmRating."+\n";
    $shortFilmCaption .= "Жанр: ".$filmGenre."\n";
    $shortFilmCaption .= $about3D."\n\n";

    $shortFilmCaption .= "СЮЖЕТ:\n";
    $shortFilmCaption .= $filmPlot."\n\n";

    $shortFilmCaption .= "В РОЛЯХ:\n";
    $shortFilmCaption .= $filmCast;

    $shortFilmCaption .= $filmDistributor;

    // Запись в файл
    $charsReplace = ['<', '>', '\\', '|', '/', '"', ':', '?', '*', '!'];
    $clearFilename = str_replace($charsReplace, '', $filmTitle);
    $filenameCardPost = "shortFilmCaptions/20".substr($articlesLinkEnd, 0, 6)." ".substr($articlesLinkEnd, 6, 2)." ".$clearFilename." (Short Film Caption).txt";

    $fd = fopen($filenameCardPost, 'w') or die("Не удалось создать файл...");
    fwrite($fd, $shortFilmCaption);
    fclose($fd);

?>