<?php

    // Точечный делитель
    $separator = str_repeat("•", 36);

    $mediumFilmCaption = "";

    $sql01 = "SELECT slogan, title, rating, genre, session_duration,
                has_3d, plot, cast, distributor, kinopoisk_link,
                articles_link_end FROM films
                    WHERE id = '".$filmID."';";
    $filmAboutObject = mysqli_query($dbConnection, $sql01);
    $filmAbout = mysqli_fetch_assoc($filmAboutObject);

    if($filmAbout["slogan"] == "—"){
        $filmSlogan = "";
    }else{
        $filmSlogan = mb_strtoupper($filmAbout["slogan"])."\n";
    }

    $filmTitle = $filmAbout["title"];
    $filmRating = $filmAbout["rating"];
    $filmGenre = $filmAbout["genre"];

    $sessionDurationInt = $filmAbout["session_duration"];
    $sessionDurationHours = (int)($sessionDurationInt / 60 / 60);
    $sessionDurationMin = ($sessionDurationInt / 60) % 60;
    if($sessionDurationMin == 0){
        $sessionDurationMinString = "ровно";
    }else{
        $sessionDurationMinString = $sessionDurationMin." мин.";
    }
    $sessionDurationString = $sessionDurationHours." ч. ".$sessionDurationMinString;

    $has3D = $filmAbout["has_3d"];
    $about3D = "";
    if($has3D == 1){
        $about3D = "Также и в 3D 👓";
    }elseif($has3D == 0){
        $about3D = "Только 2D";
    }else{
        $about3D = "Error";
    }

    $filmPlot = $filmAbout["plot"];
    $filmCast = $filmAbout["cast"];

    if($filmAbout["distributor"] == "—"){
        $filmDistributor = "";
    }else{
        $filmDistributor = "Дистрибьютор:\n".$filmAbout["distributor"]."\n\n";
    }

    $kinoPoiskLink = $filmAbout["kinopoisk_link"];

    $articlesLinkEnd = $filmAbout["articles_link_end"];

    // Запись в переменную для дальнейшей записи в файл
    // echo($filmSlogan);
    $mediumFilmCaption .= $filmSlogan;
    $mediumFilmCaption .= "«".$filmTitle."» | ".$filmRating."+\n";
    $mediumFilmCaption .= "Жанр: ".$filmGenre."\n";
    $mediumFilmCaption .= "Продолжительность сеанса: ".$sessionDurationString."\n";
    $mediumFilmCaption .= $about3D."\n\n";

    $mediumFilmCaption .= "СЮЖЕТ:\n";
    $mediumFilmCaption .= $filmPlot."\n\n";

    $mediumFilmCaption .= "В РОЛЯХ:\n";
    $mediumFilmCaption .= $filmCast."\n\n";

    $mediumFilmCaption .= $filmDistributor;

    $mediumFilmCaption .= $separator."\n";
    $mediumFilmCaption .= "🎫 Покупка билетов:\n";
    $mediumFilmCaption .= "https://супер8.рф/ https://vk.cc/clTtgW/\n";
    $mediumFilmCaption .= $separator."\n\n";

    $mediumFilmCaption .= "Фильм на КиноПоиске:\n";
    $mediumFilmCaption .= $kinoPoiskLink;

    // Запись в файл
    $charsReplace = ['<', '>', '\\', '|', '/', '"', ':', '?', '*', '!'];
    $clearFilename = str_replace($charsReplace, '', $filmTitle);
    $filenameCardPost = "mediumFilmCaptions/20".substr($articlesLinkEnd, 0, 6)." ".substr($articlesLinkEnd, 6, 2)." ".$clearFilename." (Medium Film Caption).btt";

    $fd = fopen($filenameCardPost, 'w') or die("Не удалось создать файл...");
    fwrite($fd, $mediumFilmCaption);
    fclose($fd);

?>