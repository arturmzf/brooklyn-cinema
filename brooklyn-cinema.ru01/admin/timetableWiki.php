<?php

    $resultWiki .= "[[Кинотеатр Brooklyn - ".$departmentTitleRu.". Расписание сеансов ".$period."]]\n";
    $resultWiki .= $vkEnterButton."\n";
    $resultWiki .= "\n";
    $resultWiki .= "<center>[[".$comeBackButtonTop."|1000px;nopadding|".$MainMenuPageLink."]]</center>\n";
    $resultWiki .= "<center>[[".$vkTimetableHeader."|1000px;nopadding| ]]</center>\n";
    $resultWiki .= "<br/>\n";
    $resultWiki .= "{|noborder\n";

    /*
    // Перенесено в entry.php
    // Если будет нормально работать, удалить!
    $sql08 = "SELECT ADS_DURATION FROM departments WHERE ID = '".$departmentsID."';";
    $adsDuration = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql08))["ADS_DURATION"];
    */

    $sql06 = "SELECT FILMS_ID FROM timetables WHERE DEPARTMENTS_ID = '".$departmentsID."' AND DATES_ID = '".$datesID."' GROUP BY FILMS_ID;";
    $resultFilmsListObject = mysqli_query($dbConnection, $sql06);
    $filmsAmount = mysqli_num_rows($resultFilmsListObject);

    for($i = 0; $i < $filmsAmount; $i++){

        $resultFilmsListAssoc = mysqli_fetch_assoc($resultFilmsListObject);

        $sql10 = "SELECT TITLE, HAS_3D FROM films WHERE ID = '".$resultFilmsListAssoc["FILMS_ID"]."';";
        $filmsInfo = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql10));

        if($filmsInfo["TITLE"] == null){
            continue;
        }

        if($filmsInfo["HAS_3D"] == 0){
            $sql07 = "SELECT TITLE, RATING, GENRE, SESSION_DURATION, TRAILERS_DURATION, IS_PREMIER, KINOPOISK_LINK, HAS_3D
                                    FROM films WHERE ID = '".$resultFilmsListAssoc["FILMS_ID"]."';";
            $aboutFilmResults = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql07));

            $isPremier = "";
            if($aboutFilmResults["IS_PREMIER"] == 1){
                $isPremier = " | премьера";
            }else{
                $isPremier = "";
            }

            $sessionDurationMinutesAbsolute = (int)($aboutFilmResults["SESSION_DURATION"] / 60);
            $sessionDurationHours = (int)($sessionDurationMinutesAbsolute / 60);
            $sessionDurationMinutes = $sessionDurationMinutesAbsolute % 60;

            if($sessionDurationMinutes == 0){
                $sessionDurationMinutesString = "ровно";
            }else{
                $sessionDurationMinutesString = $sessionDurationMinutes." мин.";
            }

            $adsDurationString = "";
            if($adsDuration != 0){
                $adsDurationMinutes = (int)($adsDuration / 60);
                $adsDurationSeconds = (int)($adsDuration % 60);

                if($adsDurationSeconds == 0){
                    $adsDurationString = "<br/>Продолжительность блока с рекламой и социальными роликами: '''".$adsDurationMinutes." мин. ровно'''\n";
                }elseif($adsDurationMinutes == 0) {
                    $adsDurationString = "<br/>Продолжительность блока с рекламой и социальными роликами: '''".$adsDurationSeconds." сек.'''\n";
                }else{
                    $adsDurationString = "<br/>Продолжительность блока с рекламой и социальными роликами: '''".$adsDurationMinutes." мин. ".$adsDurationSeconds." сек.'''\n";
                }
            }else{
                $adsDurationString = "";
            }

            $trailersDuration = (int)($aboutFilmResults["TRAILERS_DURATION"]);
            $trailersDurationString = "";
            if($trailersDuration != 0){
                $trailersDurationMinutes = (int)($trailersDuration / 60);
                $trailersDurationSeconds = (int)($trailersDuration % 60);

                if($trailersDurationSeconds == 0){
                    $trailersDurationString = $trailersDurationMinutes." мин. ровно";
                }elseif($trailersDurationMinutes == 0){
                    $trailersDurationString = $trailersDurationSeconds." сек.";
                }else{
                    $trailersDurationString = $trailersDurationMinutes." мин. ".$trailersDurationSeconds." сек.";
                }
            }else{
                $trailersDurationString = "отсутствует";
            }

            $resultWiki .= "|-\n";
            $resultWiki .= "! [".$aboutFilmResults["KINOPOISK_LINK"]."|🔸 «".$aboutFilmResults["TITLE"]."»] <sup> <gray>".$aboutFilmResults["RATING"]."+ | только 2D".$isPremier."</gray> </sup>\n";
            $resultWiki .= "|-\n";
            $resultWiki .= "|<br/>\n";
            $resultWiki .= "|-\n";
            $resultWiki .= "|\n";
            $resultWiki .= "<gray><b>\n";

            $sql13 = "SELECT TIME_BEGIN, TIME_END, HALLS_ID FROM timetables
                                WHERE FILMS_ID = '".$resultFilmsListAssoc["FILMS_ID"]."' AND 
                                DEPARTMENTS_ID = '".$departmentsID."' AND 
                                DATES_ID = '".$datesID."' AND IS_3D = '0' ORDER BY TIME_BEGIN;";
            $sessionsInfoObject = mysqli_query($dbConnection, $sql13);
            $sessionsAmount = mysqli_num_rows($sessionsInfoObject);

            for($count = 0; $count < $sessionsAmount; $count++){

                $sessionsInfoAssoc = mysqli_fetch_assoc($sessionsInfoObject);

                $timeSessionBegin = $sessionsInfoAssoc["TIME_BEGIN"];
                $timeSessionEnd = $sessionsInfoAssoc["TIME_END"];
                $sessionHallID = $sessionsInfoAssoc["HALLS_ID"];

                if((!empty($timeSessionBegin)) && (!empty($timeSessionEnd)) && (!empty($sessionHallID))){

                    $sql14 = "SELECT NUMBER, TITLE_EN, TITLE_RU FROM halls
                                                    WHERE ID = '".$sessionHallID."';";
                    $sessionHallInfo = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql14));

                    $hallNumber = $sessionHallInfo["NUMBER"];
                    $hallTitleEn = $sessionHallInfo["TITLE_EN"];
                    $hallTitleRu = $sessionHallInfo["TITLE_RU"];

                    if($departmentsID == 1){

                        $resultWiki .= "• ".mb_substr($timeSessionBegin, 0, 5)." | зал ".$hallTitleEn." Hall<br/>\n";

                    }else{

                        $resultWiki .= "• ".mb_substr($timeSessionBegin, 0, 5)." | зал ".$hallNumber." (".$hallTitleRu.")<br/>\n";

                    }

                }

            }

            $resultWiki .= "</b></gray>\n";
            $resultWiki .= "<br/>Продолжительность сеанса: '''".$sessionDurationHours." ч. ".$sessionDurationMinutesString."'''\n";
            $resultWiki .= "<br/>Жанр: '''".$aboutFilmResults["GENRE"]."'''\n";
            $resultWiki .= "<br/>\n";
            $resultWiki .= $adsDurationString;
            $resultWiki .= "<br/>Продолжительность блока с трейлерами: '''".$trailersDurationString."'''\n";
            $resultWiki .= "|-\n";
            $resultWiki .= "| <center>[[".$delimiter."|700px;nopadding| ]]</center>\n";

        }else{

            $sql09 = "SELECT IS_3D FROM timetables WHERE FILMS_ID = '".$resultFilmsListAssoc["FILMS_ID"]."' GROUP BY IS_3D;";
            $formatSortingObject1 = mysqli_query($dbConnection, $sql09);
            $formatSortingCount1 = mysqli_num_rows($formatSortingObject1);

            for($c = 0; $c < $formatSortingCount1; $c++){

                $formatSortingAssoc = mysqli_fetch_assoc($formatSortingObject1);

                if($formatSortingAssoc["IS_3D"] == 0){

                    $sql11 = "SELECT TITLE, RATING, GENRE, SESSION_DURATION, TRAILERS_DURATION, IS_PREMIER, KINOPOISK_LINK, HAS_3D
                                    FROM films WHERE ID = '".$resultFilmsListAssoc["FILMS_ID"]."';";
                    $aboutFilmResults = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql11));

                    $isPremier = "";
                    if($aboutFilmResults["IS_PREMIER"] == 1){
                        $isPremier = " | премьера";
                    }else{
                        $isPremier = "";
                    }

                    $sessionDurationMinutesAbsolute = (int)($aboutFilmResults["SESSION_DURATION"] / 60);
                    $sessionDurationHours = (int)($sessionDurationMinutesAbsolute / 60);
                    $sessionDurationMinutes = $sessionDurationMinutesAbsolute % 60;

                    if($sessionDurationMinutes == 0){
                        $sessionDurationMinutesString = "ровно";
                    }else{
                        $sessionDurationMinutesString = $sessionDurationMinutes." мин.";
                    }

                    $adsDurationString = "";
                    if($adsDuration != 0){
                        $adsDurationMinutes = (int)($adsDuration / 60);
                        $adsDurationSeconds = (int)($adsDuration % 60);

                        if($adsDurationSeconds == 0){
                            $adsDurationString = "<br/>Продолжительность блока с рекламой и социальными роликами: '''".$adsDurationMinutes." мин. ровно'''\n";
                        }elseif($adsDurationMinutes == 0) {
                            $adsDurationString = "<br/>Продолжительность блока с рекламой и социальными роликами: '''".$adsDurationSeconds." сек.'''\n";
                        }else{
                            $adsDurationString = "<br/>Продолжительность блока с рекламой и социальными роликами: '''".$adsDurationMinutes." мин. ".$adsDurationSeconds." сек.'''\n";
                        }
                    }else{
                        $adsDurationString = "";
                    }

                    $trailersDuration = (int)($aboutFilmResults["TRAILERS_DURATION"]);
                    $trailersDurationString = "";
                    if($trailersDuration != 0){
                        $trailersDurationMinutes = (int)($trailersDuration / 60);
                        $trailersDurationSeconds = (int)($trailersDuration % 60);

                        if($trailersDurationSeconds == 0){
                            $trailersDurationString = $trailersDurationMinutes." мин. ровно";
                        }elseif($trailersDurationMinutes == 0){
                            $trailersDurationString = $trailersDurationSeconds." сек.";
                        }else{
                            $trailersDurationString = $trailersDurationMinutes." мин. ".$trailersDurationSeconds." сек.";
                        }
                    }else{
                        $trailersDurationString = "отсутствует";
                    }

                    $resultWiki .= "|-\n";
                    $resultWiki .= "! [".$aboutFilmResults["KINOPOISK_LINK"]."|🔸 «".$aboutFilmResults["TITLE"]."» 2D] <sup> <gray>".$aboutFilmResults["RATING"]."+".$isPremier."</gray> </sup>\n";
                    $resultWiki .= "|-\n";
                    $resultWiki .= "|<br/>\n";
                    $resultWiki .= "|-\n";
                    $resultWiki .= "|\n";
                    $resultWiki .= "<gray><b>\n";

                    $sql15 = "SELECT TIME_BEGIN, TIME_END, HALLS_ID FROM timetables
                                WHERE FILMS_ID = '".$resultFilmsListAssoc["FILMS_ID"]."' AND 
                                DEPARTMENTS_ID = '".$departmentsID."' AND 
                                DATES_ID = '".$datesID."' AND IS_3D = '0' ORDER BY TIME_BEGIN;";
                    $sessionsInfoObject = mysqli_query($dbConnection, $sql15);
                    $sessionsAmount = mysqli_num_rows($sessionsInfoObject);

                    for($count = 0; $count < $sessionsAmount; $count++){

                        $sessionsInfoAssoc = mysqli_fetch_assoc($sessionsInfoObject);

                        $timeSessionBegin = $sessionsInfoAssoc["TIME_BEGIN"];
                        $timeSessionEnd = $sessionsInfoAssoc["TIME_END"];
                        $sessionHallID = $sessionsInfoAssoc["HALLS_ID"];

                        if((!empty($timeSessionBegin)) && (!empty($timeSessionEnd)) && (!empty($sessionHallID))){

                            $sql17 = "SELECT NUMBER, TITLE_EN, TITLE_RU FROM halls
                                                    WHERE ID = '".$sessionHallID."';";
                            $sessionHallInfo = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql17));

                            $hallNumber = $sessionHallInfo["NUMBER"];
                            $hallTitleEn = $sessionHallInfo["TITLE_EN"];
                            $hallTitleRu = $sessionHallInfo["TITLE_RU"];

                            if($departmentsID == 1){

                                $resultWiki .= "• ".mb_substr($timeSessionBegin, 0, 5)." | зал ".$hallTitleEn." Hall<br/>\n";

                            }else{

                                $resultWiki .= "• ".mb_substr($timeSessionBegin, 0, 5)." | зал ".$hallNumber." (".$hallTitleRu.")<br/>\n";

                            }

                        }

                    }

                    $resultWiki .= "</b></gray>\n";
                    $resultWiki .= "<br/>Продолжительность сеанса: '''".$sessionDurationHours." ч. ".$sessionDurationMinutesString."'''\n";
                    $resultWiki .= "<br/>Жанр: '''".$aboutFilmResults["GENRE"]."'''\n";
                    $resultWiki .= "<br/>\n";
                    $resultWiki .= $adsDurationString;
                    $resultWiki .= "<br/>Продолжительность блока с трейлерами: '''".$trailersDurationString."'''\n";
                    $resultWiki .= "|-\n";
                    $resultWiki .= "| <center>[[".$delimiter."|700px;nopadding| ]]</center>\n";

                }else{

                    $sql12 = "SELECT TITLE, RATING, GENRE, SESSION_DURATION, TRAILERS_DURATION, IS_PREMIER, KINOPOISK_LINK, HAS_3D
                                    FROM films WHERE ID = '".$resultFilmsListAssoc["FILMS_ID"]."';";
                    $aboutFilmResults = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql12));

                    $isPremier = "";
                    if($aboutFilmResults["IS_PREMIER"] == 1){
                        $isPremier = " | премьера";
                    }else{
                        $isPremier = "";
                    }

                    $sessionDurationMinutesAbsolute = (int)($aboutFilmResults["SESSION_DURATION"] / 60);
                    $sessionDurationHours = (int)($sessionDurationMinutesAbsolute / 60);
                    $sessionDurationMinutes = $sessionDurationMinutesAbsolute % 60;

                    if($sessionDurationMinutes == 0){
                        $sessionDurationMinutesString = "ровно";
                    }else{
                        $sessionDurationMinutesString = $sessionDurationMinutes." мин.";
                    }

                    $adsDurationString = "";
                    if($adsDuration != 0){
                        $adsDurationMinutes = (int)($adsDuration / 60);
                        $adsDurationSeconds = (int)($adsDuration % 60);

                        if($adsDurationSeconds == 0){
                            $adsDurationString = "<br/>Продолжительность блока с рекламой и социальными роликами: '''".$adsDurationMinutes." мин. ровно'''\n";
                        }elseif($adsDurationMinutes == 0) {
                            $adsDurationString = "<br/>Продолжительность блока с рекламой и социальными роликами: '''".$adsDurationSeconds." сек.'''\n";
                        }else{
                            $adsDurationString = "<br/>Продолжительность блока с рекламой и социальными роликами: '''".$adsDurationMinutes." мин. ".$adsDurationSeconds." сек.'''\n";
                        }
                    }else{
                        $adsDurationString = "";
                    }

                    $trailersDuration = (int)($aboutFilmResults["TRAILERS_DURATION"]);
                    $trailersDurationString = "";
                    if($trailersDuration != 0){
                        $trailersDurationMinutes = (int)($trailersDuration / 60);
                        $trailersDurationSeconds = (int)($trailersDuration % 60);

                        if($trailersDurationSeconds == 0){
                            $trailersDurationString = $trailersDurationMinutes." мин. ровно";
                        }elseif($trailersDurationMinutes == 0){
                            $trailersDurationString = $trailersDurationSeconds." сек.";
                        }else{
                            $trailersDurationString = $trailersDurationMinutes." мин. ".$trailersDurationSeconds." сек.";
                        }
                    }else{
                        $trailersDurationString = "отсутствует";
                    }

                    $resultWiki .= "|-\n";
                    $resultWiki .= "! [".$aboutFilmResults["KINOPOISK_LINK"]."|🔸 «".$aboutFilmResults["TITLE"]."» 3D] <sup> <gray>".$aboutFilmResults["RATING"]."+".$isPremier."</gray> </sup>\n";
                    $resultWiki .= "|-\n";
                    $resultWiki .= "|<br/>\n";
                    $resultWiki .= "|-\n";
                    $resultWiki .= "|\n";
                    $resultWiki .= "<gray><b>\n";

                    $sql16 = "SELECT TIME_BEGIN, TIME_END, HALLS_ID FROM timetables
                                WHERE FILMS_ID = '".$resultFilmsListAssoc["FILMS_ID"]."' AND 
                                DEPARTMENTS_ID = '".$departmentsID."' AND 
                                DATES_ID = '".$datesID."' AND IS_3D = '1' ORDER BY TIME_BEGIN;";
                    $sessionsInfoObject = mysqli_query($dbConnection, $sql16);
                    $sessionsAmount = mysqli_num_rows($sessionsInfoObject);

                    for($count = 0; $count < $sessionsAmount; $count++){

                        $sessionsInfoAssoc = mysqli_fetch_assoc($sessionsInfoObject);

                        $timeSessionBegin = $sessionsInfoAssoc["TIME_BEGIN"];
                        $timeSessionEnd = $sessionsInfoAssoc["TIME_END"];
                        $sessionHallID = $sessionsInfoAssoc["HALLS_ID"];

                        if((!empty($timeSessionBegin)) && (!empty($timeSessionEnd)) && (!empty($sessionHallID))){

                            $sql18 = "SELECT NUMBER, TITLE_EN, TITLE_RU FROM halls
                                                    WHERE ID = '".$sessionHallID."';";
                            $sessionHallInfo = mysqli_fetch_assoc(mysqli_query($dbConnection, $sql18));

                            $hallNumber = $sessionHallInfo["NUMBER"];
                            $hallTitleEn = $sessionHallInfo["TITLE_EN"];
                            $hallTitleRu = $sessionHallInfo["TITLE_RU"];

                            if($departmentsID == 1){

                                $resultWiki .= "• ".mb_substr($timeSessionBegin, 0, 5)." | зал ".$hallTitleEn." Hall<br/>\n";

                            }else{

                                $resultWiki .= "• ".mb_substr($timeSessionBegin, 0, 5)." | зал ".$hallNumber." (".$hallTitleRu.")<br/>\n";

                            }

                        }

                    }

                    $resultWiki .= "</b></gray>\n";
                    $resultWiki .= "<br/>Продолжительность сеанса: '''".$sessionDurationHours." ч. ".$sessionDurationMinutesString."'''\n";
                    $resultWiki .= "<br/>Жанр: '''".$aboutFilmResults["GENRE"]."'''\n";
                    $resultWiki .= "<br/>\n";
                    $resultWiki .= $adsDurationString;
                    $resultWiki .= "<br/>Продолжительность блока с трейлерами: '''".$trailersDurationString."'''\n";
                    $resultWiki .= "|-\n";
                    $resultWiki .= "| <center>[[".$delimiter."|700px;nopadding| ]]</center>\n";

                }

            }

        }

        /*
        if($resultFilmsListAssoc["IS_3D"] == 1){
            $filmSessionFormat1 = " 3D";
            $filmSessionFormat2 = "";
        }elseif(($resultFilmsListAssoc["IS_3D"] == 0) && $aboutFilmResults["HAS_3D"] == 1){
            $filmSessionFormat1 = " 2D";
            $filmSessionFormat2 = "";
        }else{
            $filmSessionFormat1 = "";
            $filmSessionFormat2 = " | только 2D";
        }
        */
    }
    /*
    $charsReplace = ['<', '>', '\\', '|', '/', '\"', ':', '?', '*'];
    $clearFilename = str_replace($charsReplace, '', $filmListResult["title"]);
    $filenameWikiELA = "cardsWiki/".time()." ".$filmListResult["id"]." ".$clearFilename." Wiki ELA.btt";
    $filenameWikiZEL = "cardsWiki/".time()." ".$filmListResult["id"]." ".$clearFilename." Wiki ZEL.btt";
    */

    $resultWiki .= "|}\n";
    $resultWiki .= "<br/>\n";
    $resultWiki .= "<center>[[".$comeBackButtonBottom."|1000px;nopadding|".$MainMenuPageLink."]]</center>\n";

    $fileNameWiki = "timetableResults/".$abbreviation." ".$yearStart.$mounthStart.$dayStart." TimetableWiki ".time().".btt";

    $fdWiki = fopen($fileNameWiki, 'w') or die("Не удалось создать файл...");
    fwrite($fdWiki, $resultWiki);
    fclose($fdWiki);

?>